<?php


namespace Vallarj\Mezzio\WebService\Handler;


use Vallarj\Mezzio\WebService\Exception\InvalidResponseTypeException;
use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Mezzio\OAuth\ResourceServer\Middleware\OAuthResourceServerMiddleware;
use Vallarj\Mezzio\WebService\Middleware\WebServiceMiddleware;
use Vallarj\Mezzio\WebService\Response\JsonApiResponse;
use JsonSerializable;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vallarj\JsonApi\Decoder\ResultInterface;
use Vallarj\JsonApi\DecoderInterface;
use Vallarj\JsonApi\EncoderInterface;
use Vallarj\JsonApi\Error\Error;
use Vallarj\JsonApi\Error\ErrorDocument;
use Vallarj\JsonApi\Exception\InvalidFormatException;

abstract class AbstractJsonApiRequestHandler implements RequestHandlerInterface
{
    /** @var EncoderInterface */
    private $encoder;

    /** @var DecoderInterface */
    private $decoder;

    /** @var ServerRequestInterface */
    private $currentRequest;

    /** @var RbacUser|null */
    private $currentUser;

    /** @var array */
    private $currentScopes;

    /** @var string[] */
    protected $authorizedMethods = [
        'GETLIST',
        'GET',
        'POST',
        'PATCH',
        'DELETE'
    ];

    /**
     * @inheritDoc
     * @throws InvalidResponseTypeException
     */
    final public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $this->currentRequest = $request;

        $this->currentUser = $request->getAttribute(WebServiceMiddleware::USER_ATTRIBUTE);
        $this->currentScopes = $request->getAttribute(OAuthResourceServerMiddleware::SCOPES_ATTRIBUTE);
        $hasAccessToken = $request->getAttribute(OAuthResourceServerMiddleware::HAS_ACCESS_TOKEN_ATTRIBUTE);
        $validAccessToken = $request->getAttribute(OAuthResourceServerMiddleware::ACCESS_TOKEN_VALID_ATTRIBUTE);

        $this->encoder = $request->getAttribute(WebServiceMiddleware::ENCODER_ATTRIBUTE);
        $this->decoder = $request->getAttribute(WebServiceMiddleware::DECODER_ATTRIBUTE);

        $method = $request->getMethod();
        $return = null;

        $id = $request->getAttribute('id');
        if ($method === "GET" && !$id) {
            $method = "GETLIST";
        }

        // If method needs to be authorized
        if (in_array($method, $this->authorizedMethods)) {
            if (!$hasAccessToken || !$validAccessToken) {
                // If access token is invalid
                $return = $this->invalidAccessTokenResponse();
            } else if (is_null($this->currentUser) || !$this->isAuthorized($this->currentUser, $method)) {
                // else if user is null or not authorized
                $return = $this->unauthorizedResponse();
            }
        }

        if (is_null($return)) {
            switch ($method) {
                case "GET":
                    $return = $this->handleGet($id);
                    break;
                case "GETLIST":
                    $return = $this->handleGetList();
                    break;
                case "POST":
                    $return = $this->handlePost($request->getBody()->getContents());
                    break;
                case "PATCH":
                    if (!$id) {
                        $return = $this->methodNotAllowedResponse();
                    } else {
                        $return = $this->handlePatch(
                            $request->getAttribute('id'),
                            $request->getBody()->getContents()
                        );
                    }
                    break;
                case "DELETE":
                    if (!$id) {
                        $return = $this->methodNotAllowedResponse();
                    } else {
                        $return = $this->handleDelete(
                            $request->getAttribute('id'),
                            $request->getBody()->getContents()
                        );
                    }
                    break;
                default:
                    $return = $this->methodNotAllowedResponse();
                    break;
            }
        }

        if ($return instanceof ResponseInterface) {
            return $return;
        } else if ($return instanceof JsonSerializable || is_array($return) || is_object($return)) {
            return new JsonApiResponse($return, $method === "POST" ? 201 : 200);
        } else {
            throw new InvalidResponseTypeException("Invalid response type returned in handler.");
        }
    }

    /**
     * Returns the current handled request
     *
     * @return ServerRequestInterface
     */
    final public function getCurrentRequest(): ServerRequestInterface
    {
        return $this->currentRequest;
    }

    /**
     * Returns the current user
     *
     * @return RbacUser|null
     */
    final public function getBaseUser(): ?RbacUser
    {
        return $this->currentUser;
    }

    /**
     * Returns true if the current request has the specified scope
     *
     * @param string $scope
     * @return bool
     */
    final public function hasScope(string $scope): bool
    {
        return in_array($scope, $this->currentScopes);
    }

    /**
     * Returns the query parameters
     *
     * @return array
     */
    final public function getQueryParams(): array
    {
        return $this->currentRequest->getQueryParams();
    }

    /**
     * Encodes an object into a JSON API document
     *
     * @param mixed $resource
     * @param string[] $schemas
     * @param array $includedKeys
     * @param callable|null $showProperty
     * @param array $meta
     * @return JsonSerializable
     */
    final public function encode(
        $resource,
        array $schemas,
        array $includedKeys = [],
        ?callable $showProperty = null,
        array $meta = []
    ): JsonSerializable {
        return $this->encoder->encode($resource, $schemas, $includedKeys, $showProperty, $meta);
    }

    /**
     * Decodes a POST document into a new object from a compatible schema
     *
     * @param string $data
     * @param string[] $schemas
     * @param bool $allowEphemeralId
     * @return ResultInterface
     * @throws InvalidFormatException
     */
    final public function decodePostResource(
        string $data,
        array $schemas,
        bool $allowEphemeralId = false
    ): ResultInterface {
        return $this->decoder->decodePostResource($data, $schemas, $allowEphemeralId);
    }

    /**
     * Decodes a PATCH document into a new object from a compatible schema
     *
     * @param string $data
     * @param string[] $schemas
     * @param mixed $expectedId If not null, decoder will check if provided ID matches the expected ID.
     * @return ResultInterface
     * @throws InvalidFormatException
     */
    final public function decodePatchResource(
        string $data,
        array $schemas,
        $expectedId = null
    ): ResultInterface {
        return $this->decoder->decodePatchResource($data, $schemas, $expectedId);
    }

    /**
     * Decodes a To-one relationship request into a new object from a compatible schema
     *
     * @param string $data
     * @param string[] $schemas
     * @return mixed
     * @throws InvalidFormatException
     */
    final public function decodeToOneRelationshipRequest(string $data, array $schemas)
    {
        return $this->decoder->decodeToOneRelationshipRequest($data, $schemas);
    }

    /**
     * Decodes a To-many relationship request into new objects from a compatible schema
     *
     * @param string $data
     * @param string[] $schemas
     * @return mixed
     * @throws InvalidFormatException
     */
    final public function decodeToManyRelationshipRequest(string $data, array $schemas)
    {
        return $this->decoder->decodeToManyRelationshipRequest($data, $schemas);
    }

    /**
     * Returns an error document for invalid access token
     *
     * @return ErrorDocument
     */
    final public function invalidAccessTokenResponse(): ErrorDocument
    {
        $errDoc = new ErrorDocument(401);
        $error = new Error();
        $error->setTitle('Unauthorized')
            ->setDetail('You have provided an invalid access token.');
        $errDoc->addError($error);

        return $errDoc;
    }

    /**
     * Returns an error document for unauthorized access
     *
     * @return ErrorDocument
     */
    final public function unauthorizedResponse(): ErrorDocument
    {
        $errDoc = new ErrorDocument(403);
        $error = new Error();
        $error->setTitle('Forbidden')
            ->setDetail('You are unauthorized to access this resource.');
        $errDoc->addError($error);

        return $errDoc;
    }

    /**
     * Returns a 404 Not Found error document
     *
     * @return ErrorDocument
     */
    final public function notFoundResponse(): ErrorDocument
    {
        $errDoc = new ErrorDocument(404);
        $error = new Error();
        $error->setTitle('Not Found')
            ->setDetail('Resource not found.');
        $errDoc->addError($error);

        return $errDoc;
    }

    /**
     * Returns a 405 Method Not Allowed error document
     *
     * @return ErrorDocument
     */
    final public function methodNotAllowedResponse(): ErrorDocument
    {
        $errDoc = new ErrorDocument(405);
        $error = new Error();
        $error->setTitle('Method Not Allowed')
            ->setDetail('This method is currently unsupported.');
        $errDoc->addError($error);

        return $errDoc;
    }

    /**
     * Handles a GETLIST request
     *
     * @return ResponseInterface|JsonSerializable|array|object
     */
    public function handleGetList()
    {
        return $this->methodNotAllowedResponse();
    }

    /**
     * Handles a GET request
     *
     * @param mixed $id
     * @return ResponseInterface|JsonSerializable|array|object
     */
    public function handleGet($id)
    {
        return $this->methodNotAllowedResponse();
    }

    /**
     * Handles a POST request
     *
     * @param mixed $data
     * @return ResponseInterface|JsonSerializable|array|object
     */
    public function handlePost($data)
    {
        return $this->methodNotAllowedResponse();
    }

    /**
     * Handles a PATCH request
     *
     * @param mixed $id
     * @param mixed $data
     * @return ResponseInterface|JsonSerializable|array|object
     */
    public function handlePatch($id, $data)
    {
        return $this->methodNotAllowedResponse();
    }

    /**
     * Handles a DELETE request
     *
     * @param mixed $id
     * @param mixed $data
     * @return ResponseInterface|JsonSerializable|array|object
     */
    public function handleDelete($id, $data)
    {
        return $this->methodNotAllowedResponse();
    }

    /**
     * Returns true if access is authorized
     *
     * @param RbacUser $user
     * @param string $method
     * @return bool
     */
    protected function isAuthorized(RbacUser $user, string $method): bool
    {
        return true;
    }
}
