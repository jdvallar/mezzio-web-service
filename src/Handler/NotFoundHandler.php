<?php


namespace Vallarj\Mezzio\WebService\Handler;


use Vallarj\Mezzio\WebService\Response\JsonApiResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vallarj\JsonApi\Error\Error;
use Vallarj\JsonApi\Error\ErrorDocument;

final class NotFoundHandler implements RequestHandlerInterface
{
    /**
     * @inheritDoc
     */
    public function handle(ServerRequestInterface $request): ResponseInterface
    {
        $errDoc = new ErrorDocument(404);
        $error = new Error();
        $error->setTitle('Not Found')
            ->setDetail('Resource not found.');
        $errDoc->addError($error);

        return new JsonApiResponse($errDoc);
    }
}
