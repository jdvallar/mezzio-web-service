<?php


namespace Vallarj\Mezzio\WebService;


class ConfigPostProcessor
{
    private const CONFIG_PHP_JSON_API = 'php-json-api';
    private const CONFIG_PHP_OBJECT_MAPPER = 'php-object-mapper';

    /**
     * WebService configuration post processor
     *
     * @param array $config
     * @return array
     */
    public function __invoke(array $config): array
    {
        $mappers = $config['web-service']['mappers'] ?? [];
        $schemas = $config['web-service']['schemas'] ?? [];
        $userDTOForwardMapper = $mappers['user-forward-mapper'] ?? null;
        $userDTOReverseMapper = $mappers['user-reverse-mapper'] ?? null;
        $userSchema = $schemas['user'] ?? null;
        $adminUserSchema = $schemas['admin-user'] ?? null;

        if (!is_null($userDTOForwardMapper)) {
            if (!in_array($userDTOForwardMapper, $config[self::CONFIG_PHP_OBJECT_MAPPER]['mappers'])) {
                $config[self::CONFIG_PHP_OBJECT_MAPPER]['mappers'][] = $userDTOForwardMapper;
            }
        }

        if (!is_null($userDTOReverseMapper)) {
            if (!in_array($userDTOReverseMapper, $config[self::CONFIG_PHP_OBJECT_MAPPER]['mappers'])) {
                $config[self::CONFIG_PHP_OBJECT_MAPPER]['mappers'][] = $userDTOReverseMapper;
            }
        }

        if (!is_null($userSchema)) {
            if (!in_array($userSchema, $config[self::CONFIG_PHP_JSON_API]['schemas'])) {
                $config[self::CONFIG_PHP_JSON_API]['schemas'][] = $userSchema;
            }
        }

        if (!is_null($adminUserSchema)) {
            if (!in_array($adminUserSchema, $config[self::CONFIG_PHP_JSON_API]['schemas'])) {
                $config[self::CONFIG_PHP_JSON_API]['schemas'][] = $adminUserSchema;
            }
        }

        return $config;
    }
}
