<?php


namespace Vallarj\Mezzio\WebService\Rbac\Handler;


use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Mezzio\WebService\Handler\AbstractJsonApiRequestHandler;
use Vallarj\Mezzio\WebService\Rbac\Schema\PermissionSchema;
use Vallarj\Mezzio\WebService\Rbac\Service\PermissionServiceInterface;

class PermissionHandler extends AbstractJsonApiRequestHandler
{
    /** @var PermissionServiceInterface */
    private $permissionService;

    /** @var string */
    private $authorizedPermission;

    /**
     * PermissionHandler constructor.
     *
     * @param PermissionServiceInterface $permissionService
     * @param string $authorizedPermission
     */
    public function __construct(PermissionServiceInterface $permissionService, string $authorizedPermission)
    {
        $this->permissionService = $permissionService;
        $this->authorizedPermission = $authorizedPermission;
    }

    /**
     * @inheritDoc
     */
    public function isAuthorized(RbacUser $user, string $method): bool
    {
        return $user->hasPermission($this->authorizedPermission);
    }

    /**
     * @inheritDoc
     */
    public function handleGetList()
    {
        $permissions = $this->permissionService->fetchAllPermissions();
        return $this->encode($permissions, [PermissionSchema::class]);
    }

    /**
     * @inheritDoc
     */
    public function handleGet($id)
    {
        $permission = $this->permissionService->fetchPermission($id);
        return $this->encode($permission, [PermissionSchema::class]);
    }
}
