<?php


namespace Vallarj\Mezzio\WebService\Rbac\Handler;


use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Mezzio\WebService\Handler\AbstractJsonApiRequestHandler;
use Vallarj\Mezzio\WebService\Rbac\DTO\RoleDTO;
use Vallarj\Mezzio\WebService\Rbac\Exception\RoleAlreadyExistsException;
use Vallarj\Mezzio\WebService\Rbac\Schema\RoleSchema;
use Vallarj\Mezzio\WebService\Rbac\Service\RoleServiceInterface;
use Laminas\Diactoros\Response\EmptyResponse;
use Vallarj\JsonApi\Error\Error;
use Vallarj\JsonApi\Error\ErrorDocument;
use Vallarj\JsonApi\Error\Source\Pointer;
use Vallarj\JsonApi\Exception\InvalidFormatException;

class RoleHandler extends AbstractJsonApiRequestHandler
{
    /** @var RoleServiceInterface */
    private $roleService;

    /** @var string */
    private $roleReadPermission;

    /** @var string */
    private $roleManagePermission;

    /**
     * RoleHandler constructor.
     *
     * @param RoleServiceInterface $roleService
     * @param string $roleReadPermission
     * @param string $roleManagePermission
     */
    public function __construct(
        RoleServiceInterface $roleService,
        string $roleReadPermission,
        string $roleManagePermission
    ) {
        $this->roleService = $roleService;
        $this->roleReadPermission = $roleReadPermission;
        $this->roleManagePermission = $roleManagePermission;
    }

    /**
     * @inheritDoc
     */
    protected function isAuthorized(RbacUser $user, string $method): bool
    {
        switch ($method) {
            case "GETLIST":
            case "GET":
                return (
                    $user->hasPermission($this->roleManagePermission)
                    || $user->hasPermission($this->roleReadPermission)
                );
            case "POST":
            case "PATCH":
            case "DELETE":
                return $user->hasPermission($this->roleManagePermission);
            default:
                return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function handleGetList()
    {
        $roles = $this->roleService->fetchAllRoles();
        return $this->encode($roles, [RoleSchema::class]);
    }

    /**
     * @inheritDoc
     */
    public function handleGet($id)
    {
        $role = $this->roleService->fetchRole($id);
        return $this->encode($role, [RoleSchema::class]);
    }

    /**
     * @inheritDoc
     * @throws InvalidFormatException
     */
    public function handlePost($data)
    {
        $result = $this->decodePostResource($data, [RoleSchema::class], true);

        if ($result->hasValidationErrors()) {
            return $result->getErrorDocument();
        }

        /** @var RoleDTO $roleDTO */
        $roleDTO = $result->getResult();
        if (empty(trim($roleDTO->getId()))) {
            return $this->roleIdRequiredResponse();
        }

        try {
            $roleDTO = $this->roleService->createRole($result->getResult());
        } catch (RoleAlreadyExistsException $exception) {
            return $this->roleWithIdAlreadyExistsResponse();
        }

        return $this->encode($roleDTO, [RoleSchema::class]);
    }

    /**
     * @inheritDoc
     * @throws InvalidFormatException
     */
    public function handlePatch($id, $data)
    {
        $result = $this->decodePatchResource($data, [RoleSchema::class], $id);

        if ($result->hasValidationErrors()) {
            return $result->getErrorDocument();
        }

        $this->roleService->updateRole($id, $result->getResult(), $result->getModifiedProperties());
        return new EmptyResponse();
    }

    /**
     * @inheritDoc
     */
    public function handleDelete($id, $data)
    {
        $this->roleService->deleteRole($id);
        return new EmptyResponse();
    }

    /**
     * Returns an error response when role with ID already exists.
     *
     * @return ErrorDocument
     */
    private function roleWithIdAlreadyExistsResponse(): ErrorDocument
    {
        $errorDocument = new ErrorDocument("422");
        $errorDocument->addError(
            (new Error())->setSource(new Pointer("/data/id"))
                ->setId("Role with ID already exists.")
        );

        return $errorDocument;
    }

    /**
     * Returns an error response when role with ID is empty.
     *
     * @return ErrorDocument
     */
    private function roleIdRequiredResponse(): ErrorDocument
    {
        $errorDocument = new ErrorDocument("422");
        $errorDocument->addError(
            (new Error())->setSource(new Pointer("/data/id"))
                ->setId("ID is required.")
        );

        return $errorDocument;
    }
}
