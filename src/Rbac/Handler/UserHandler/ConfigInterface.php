<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Rbac\Handler\UserHandler;


interface ConfigInterface
{
    /**
     * Returns the regular user schema
     *
     * @return string
     */
    public function getUserSchema(): string;

    /**
     * Returns the admin user schema
     *
     * @return string
     */
    public function getAdminUserSchema(): string;

    /**
     * Returns the regular user permission
     *
     * @return string
     */
    public function getRegularUserPermission(): string;

    /**
     * Returns the user admin permission
     *
     * @return string
     */
    public function getUserAdminPermission(): string;

    /**
     * Returns the user read permission
     *
     * @return string
     */
    public function getUserReadPermission(): string;

    /**
     * Returns the user create permission
     *
     * @return string
     */
    public function getUserCreatePermission(): string;

    /**
     * Returns the user update permission
     *
     * @return string
     */
    public function getUserUpdatePermission(): string;

    /**
     * Returns the user activate permission
     *
     * @return string
     */
    public function getUserActivatePermission(): string;

    /**
     * Returns the user deactivate permission
     *
     * @return string
     */
    public function getUserDeactivatePermission(): string;

    /**
     * Returns the user reset password permission
     *
     * @return string
     */
    public function getUserResetPasswordPermission(): string;
}
