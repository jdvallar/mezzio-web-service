<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Rbac\Handler\UserHandler;


class Config implements ConfigInterface
{
    /** @var string */
    private $userSchema;

    /** @var string */
    private $adminUserSchema;

    /** @var string */
    private $regularUserPermission;

    /** @var string */
    private $userAdminPermission;

    /** @var string */
    private $userReadPermission;

    /** @var string */
    private $userCreatePermission;

    /** @var string */
    private $userUpdatePermission;

    /** @var string */
    private $userActivatePermission;

    /** @var string */
    private $userDeactivatePermission;

    /** @var string */
    private $userResetPasswordPermission;

    /**
     * Config constructor.
     *
     * @param string $userSchema
     * @param string $adminUserSchema
     * @param string $regularUserPermission
     * @param string $userAdminPermission
     * @param string $userReadPermission
     * @param string $userCreatePermission
     * @param string $userUpdatePermission
     * @param string $userActivatePermission
     * @param string $userDeactivatePermission
     * @param string $userResetPasswordPermission
     */
    public function __construct(
        string $userSchema,
        string $adminUserSchema,
        string $regularUserPermission,
        string $userAdminPermission,
        string $userReadPermission,
        string $userCreatePermission,
        string $userUpdatePermission,
        string $userActivatePermission,
        string $userDeactivatePermission,
        string $userResetPasswordPermission
    ) {
        $this->userSchema = $userSchema;
        $this->adminUserSchema = $adminUserSchema;
        $this->regularUserPermission = $regularUserPermission;
        $this->userAdminPermission = $userAdminPermission;
        $this->userReadPermission = $userReadPermission;
        $this->userCreatePermission = $userCreatePermission;
        $this->userUpdatePermission = $userUpdatePermission;
        $this->userActivatePermission = $userActivatePermission;
        $this->userDeactivatePermission = $userDeactivatePermission;
        $this->userResetPasswordPermission = $userResetPasswordPermission;
    }

    /**
     * @inheritDoc
     */
    public function getUserSchema(): string
    {
        return $this->userSchema;
    }

    /**
     * @inheritDoc
     */
    public function getAdminUserSchema(): string
    {
        return $this->adminUserSchema;
    }

    /**
     * @inheritDoc
     */
    public function getRegularUserPermission(): string
    {
        return $this->regularUserPermission;
    }

    /**
     * @inheritDoc
     */
    public function getUserAdminPermission(): string
    {
        return $this->userAdminPermission;
    }

    /**
     * @inheritDoc
     */
    public function getUserReadPermission(): string
    {
        return $this->userReadPermission;
    }

    /**
     * @inheritDoc
     */
    public function getUserCreatePermission(): string
    {
        return $this->userCreatePermission;
    }

    /**
     * @inheritDoc
     */
    public function getUserUpdatePermission(): string
    {
        return $this->userUpdatePermission;
    }

    /**
     * @inheritDoc
     */
    public function getUserActivatePermission(): string
    {
        return $this->userActivatePermission;
    }

    /**
     * @inheritDoc
     */
    public function getUserDeactivatePermission(): string
    {
        return $this->userDeactivatePermission;
    }

    /**
     * @inheritDoc
     */
    public function getUserResetPasswordPermission(): string
    {
        return $this->userResetPasswordPermission;
    }
}
