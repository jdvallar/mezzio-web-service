<?php


namespace Vallarj\Mezzio\WebService\Rbac\Handler;


use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Mezzio\WebService\Exception\InvalidCurrentPasswordException;
use Vallarj\Mezzio\WebService\Exception\UserWithUsernameAlreadyExistsException;
use Vallarj\Mezzio\WebService\Handler\AbstractJsonApiRequestHandler;
use Vallarj\Mezzio\WebService\Rbac\DTO\AbstractUserDTO;
use Vallarj\Mezzio\WebService\Rbac\Handler\UserHandler\ConfigInterface;
use Vallarj\Mezzio\WebService\Rbac\Service\UserServiceInterface;
use Laminas\Diactoros\Response\EmptyResponse;
use Vallarj\JsonApi\Error\Error;
use Vallarj\JsonApi\Error\ErrorDocument;
use Vallarj\JsonApi\Error\Source\AttributePointer;
use Vallarj\JsonApi\Exception\InvalidFormatException;

class UserHandler extends AbstractJsonApiRequestHandler
{
    /** @var UserServiceInterface */
    private $userService;

    /** @var ConfigInterface */
    private $config;

    /**
     * UserHandler constructor.
     *
     * @param UserServiceInterface $userService
     * @param ConfigInterface $config
     */
    public function __construct(
        UserServiceInterface $userService,
        ConfigInterface $config
    ) {
        $this->userService = $userService;
        $this->config = $config;
    }

    /**
     * @inheritDoc
     */
    protected function isAuthorized(RbacUser $user, string $method): bool
    {
        switch ($method) {
            case "GETLIST":
                return (
                    $user->hasPermission($this->config->getUserAdminPermission())
                    || $user->hasPermission($this->config->getUserReadPermission())
                );
            case "GET":
                return (
                    $user->hasPermission($this->config->getUserAdminPermission())
                    || $user->hasPermission($this->config->getRegularUserPermission())
                    || $user->hasPermission($this->config->getUserReadPermission())
                );
            case "POST":
                return $user->hasPermission($this->config->getUserCreatePermission());
            case "PATCH":
                return (
                    $user->hasPermission($this->config->getRegularUserPermission())
                    || $user->hasPermission($this->config->getUserUpdatePermission())
                    || $user->hasPermission($this->config->getUserActivatePermission())
                    || $user->hasPermission($this->config->getUserDeactivatePermission())
                    || $user->hasPermission($this->config->getUserResetPasswordPermission())
                );
            case "DELETE":
            default:
                return false;
        }
    }

    /**
     * @inheritDoc
     */
    public function handleGetList()
    {
        $params = $this->getQueryParams();

        $length = isset($params['n']) ? (int)$params['n'] : null;
        $lastId = $params['l'] ?? null;
        $searchQuery = $params['s'] ?? null;

        $users = $this->userService->fetchUsers($this->getBaseUser(), $searchQuery, $lastId, $length);
        return $this->encode(
            $users,
            [$this->config->getAdminUserSchema()],
            ['roles'],
            [$this, 'filterUserResetProperties']
        );
    }

    /**
     * @inheritDoc
     */
    public function handleGet($id)
    {
        $currentUser = $this->getBaseUser();
        if (
            $id === "me"
            || ($id === $currentUser->getId() && !$currentUser->hasPermission($this->config->getUserAdminPermission()))
        ) {
            $user = $this->userService->fetchUser($currentUser, $currentUser->getId());
            return $this->encode($user, [$this->config->getUserSchema()], ['roles']);
        } else if (
            $currentUser->hasPermission($this->config->getUserAdminPermission())
            || $currentUser->hasPermission($this->config->getUserReadPermission())
        ) {
            $user = $this->userService->fetchUser($currentUser, $id);
            if (!$user) {
                return $this->notFoundResponse();
            }

            return $this->encode(
                $user,
                [$this->config->getAdminUserSchema()],
                ['roles'],
                [$this, 'filterUserResetProperties']
            );
        }

        return $this->unauthorizedResponse();
    }

    /**
     * @inheritDoc
     * @throws InvalidFormatException
     */
    public function handlePost($data)
    {
        $result = $this->decodePostResource($data, [$this->config->getAdminUserSchema()]);

        if ($result->hasValidationErrors()) {
            return $result->getErrorDocument();
        }

        try {
            $userDTO = $this->userService->createUser($this->getBaseUser(), $result->getResult());
        } catch (UserWithUsernameAlreadyExistsException $exception) {
            return $this->userWithUsernameAlreadyExistsResponse();
        }

        return $this->encode($userDTO, [$this->config->getAdminUserSchema()], [], [$this, 'filterUserResetProperties']);
    }

    /**
     * @inheritDoc
     * @throws InvalidFormatException
     * @throws InvalidCurrentPasswordException
     */
    public function handlePatch($id, $data)
    {
        $currentUser = $this->getBaseUser();
        if ($id === "me") {
            $result = $this->decodePatchResource($data, [$this->config->getUserSchema()], $id);

            if ($result->hasValidationErrors()) {
                return $result->getErrorDocument();
            }

            try {
                $this->userService->updateUser(
                    $currentUser,
                    $currentUser->getId(),
                    $result->getResult(),
                    $result->getModifiedProperties()
                );
            } catch (InvalidCurrentPasswordException $e) {
                return $this->incorrectOldPasswordResponse();
            }

            return new EmptyResponse();
        } else if (
            $currentUser->hasPermission($this->config->getUserUpdatePermission())
            || $currentUser->hasPermission($this->config->getUserActivatePermission())
            || $currentUser->hasPermission($this->config->getUserDeactivatePermission())
            || $currentUser->hasPermission($this->config->getUserResetPasswordPermission())
        ) {
            $result = $this->decodePatchResource($data, [$this->config->getAdminUserSchema()], $id);

            if ($result->hasValidationErrors()) {
                return $result->getErrorDocument();
            }

            /** @var AbstractUserDTO $userDTO */
            $userDTO = $result->getResult();
            $modifiedProperties = $result->getModifiedProperties();

            // Check if active has been changed
            if (in_array('active', $modifiedProperties)) {
                if (
                    $userDTO->getActive() === true
                    && !$currentUser->hasPermission($this->config->getUserActivatePermission())
                ) {
                    return $this->unauthorizedResponse();
                } else if (
                    $userDTO->getActive() === false
                    && !$currentUser->hasPermission($this->config->getUserDeactivatePermission())
                ) {
                    return $this->unauthorizedResponse();
                }
            }

            // Check if resetPassword is true
            if (
                in_array('resetPassword', $modifiedProperties)
                && $userDTO->getResetPassword() === true
                && !$currentUser->hasPermission($this->config->getUserResetPasswordPermission())
            ) {
                return $this->unauthorizedResponse();
            }

            // Check if updating other fields
            if (
                count(array_diff($modifiedProperties, ['active', 'resetPassword']))
                && !$currentUser->hasPermission($this->config->getUserUpdatePermission())
            ) {
                return $this->unauthorizedResponse();
            }

            $this->userService->updateUser($currentUser, $id, $userDTO, $modifiedProperties);

            if (
                in_array('resetPassword', $modifiedProperties)
                && $userDTO->getResetPassword() === true
            ) {
                $updated = $this->userService->fetchUser($currentUser, $id);
                return $this->encode(
                    $updated,
                    [$this->config->getAdminUserSchema()],
                    [],
                    [$this, 'showUserResetPropertiesOnly']
                );
            } else {
                return new EmptyResponse();
            }
        } else {
            // Regular user trying to access PATCH endpoint of another user
            return $this->unauthorizedResponse();
        }
    }

    /**
     * Filters user reset password related properties
     *
     * @param string $resourceType
     * @param string $property
     * @return bool
     */
    public function filterUserResetProperties(string $resourceType, string $property): bool
    {
        if (
            $resourceType === 'users'
            && ($property === 'temporaryPassword' || $property === 'usesTemporaryPassword')
        ) {
            return $this->getBaseUser()->hasPermission($this->config->getUserResetPasswordPermission());
        }

        return true;
    }

    /**
     * Shows only user reset password related properties
     *
     * @param string $resourceType
     * @param string $property
     * @return bool
     */
    public function showUserResetPropertiesOnly(string $resourceType, string $property): bool
    {
        return (
            $resourceType === 'users'
            && ($property === 'temporaryPassword' || $property === 'usesTemporaryPassword')
        );
    }

    /**
     * Returns an error response when username already exists
     *
     * @return ErrorDocument
     */
    private function userWithUsernameAlreadyExistsResponse(): ErrorDocument
    {
        $errorDocument = new ErrorDocument("422");
        $errorDocument->addError(
            (new Error())->setSource(new AttributePointer("username"))
                ->setDetail("Username already exists.")
        );

        return $errorDocument;
    }

    /**
     * Returns an error response when password is incorrect
     *
     * @return ErrorDocument
     */
    private function incorrectOldPasswordResponse(): ErrorDocument
    {
        $errorDocument = new ErrorDocument("422");
        $errorDocument->addError(
            (new Error())->setSource(new AttributePointer("oldPassword"))
                ->setDetail("Password is incorrect.")
        );

        return $errorDocument;
    }
}
