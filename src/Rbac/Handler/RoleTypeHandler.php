<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Rbac\Handler;


use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Mezzio\WebService\Handler\AbstractJsonApiRequestHandler;
use Vallarj\Mezzio\WebService\Rbac\Schema\RoleTypeSchema;
use Vallarj\Mezzio\WebService\Rbac\Service\RoleTypeServiceInterface;

class RoleTypeHandler extends AbstractJsonApiRequestHandler
{
    /** @var RoleTypeServiceInterface */
    private $roleTypeService;

    /** @var string */
    private $authorizedPermission;

    /**
     * RoleTypeHandler constructor.
     *
     * @param RoleTypeServiceInterface $roleTypeService
     * @param string $authorizedPermission
     */
    public function __construct(RoleTypeServiceInterface $roleTypeService, string $authorizedPermission)
    {
        $this->roleTypeService = $roleTypeService;
        $this->authorizedPermission = $authorizedPermission;
    }

    /**
     * @inheritDoc
     */
    protected function isAuthorized(RbacUser $user, string $method): bool
    {
        return $user->hasPermission($this->authorizedPermission);
    }

    /**
     * @inheritDoc
     */
    public function handleGetList()
    {
        $roleTypes = $this->roleTypeService->fetchAllRoleTypes();
        return $this->encode($roleTypes, [RoleTypeSchema::class]);
    }

    /**
     * @inheritDoc
     */
    public function handleGet($id)
    {
        $roleType = $this->roleTypeService->fetchRoleType($id);
        return $this->encode($roleType, [RoleTypeSchema::class]);
    }
}
