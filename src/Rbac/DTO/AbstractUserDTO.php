<?php


namespace Vallarj\Mezzio\WebService\Rbac\DTO;


abstract class AbstractUserDTO
{
    /** @var string */
    private $id;

    /** @var RoleDTO[] */
    private $roles;

    /** @var string */
    private $username;

    /** @var string|null */
    private $oldPassword;

    /** @var string|null */
    private $password;

    /** @var string */
    private $firstName;

    /** @var string|null */
    private $middleName;

    /** @var string */
    private $lastName;

    /** @var bool */
    private $active;

    /** @var bool|null */
    private $resetPassword;

    /** @var string|null */
    private $temporaryPassword;

    /** @var bool */
    private $usesTemporaryPassword;

    /**
     * AbstractUserDTO constructor.
     */
    public function __construct()
    {
        $this->roles = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return static
     */
    public function setId(string $id): AbstractUserDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return RoleDTO[]
     */
    public function getRoles(): array
    {
        return $this->roles;
    }

    /**
     * @param RoleDTO[] $roles
     * @return static
     */
    public function setRoles(array $roles): AbstractUserDTO
    {
        $this->roles = $roles;
        return $this;
    }

    /**
     * @param RoleDTO $role
     * @return static
     */
    public function addRole(RoleDTO $role): AbstractUserDTO
    {
        $this->roles[] = $role;
        return $this;
    }

    /**
     * @return string
     */
    public function getUsername(): string
    {
        return $this->username;
    }

    /**
     * @param string $username
     * @return static
     */
    public function setUsername(string $username): AbstractUserDTO
    {
        $this->username = $username;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getOldPassword(): ?string
    {
        return $this->oldPassword;
    }

    /**
     * @param string|null $oldPassword
     * @return AbstractUserDTO
     */
    public function setOldPassword(?string $oldPassword): AbstractUserDTO
    {
        $this->oldPassword = $oldPassword;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPassword(): ?string
    {
        return $this->password;
    }

    /**
     * @param string|null $password
     * @return static
     */
    public function setPassword(?string $password): AbstractUserDTO
    {
        $this->password = $password;
        return $this;
    }

    /**
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->firstName;
    }

    /**
     * @param string $firstName
     * @return AbstractUserDTO
     */
    public function setFirstName(string $firstName): AbstractUserDTO
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @param string|null $middleName
     * @return AbstractUserDTO
     */
    public function setMiddleName(?string $middleName): AbstractUserDTO
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * @return string
     */
    public function getLastName(): string
    {
        return $this->lastName;
    }

    /**
     * @param string $lastName
     * @return AbstractUserDTO
     */
    public function setLastName(string $lastName): AbstractUserDTO
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     * @return static
     */
    public function setActive(bool $active): AbstractUserDTO
    {
        $this->active = $active;
        return $this;
    }

    /**
     * @return bool|null
     */
    public function getResetPassword(): ?bool
    {
        return $this->resetPassword;
    }

    /**
     * @param bool|null $resetPassword
     * @return AbstractUserDTO
     */
    public function setResetPassword(?bool $resetPassword): AbstractUserDTO
    {
        $this->resetPassword = $resetPassword;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTemporaryPassword(): ?string
    {
        return $this->temporaryPassword;
    }

    /**
     * @param string|null $temporaryPassword
     * @return static
     */
    public function setTemporaryPassword(?string $temporaryPassword): AbstractUserDTO
    {
        $this->temporaryPassword = $temporaryPassword;
        return $this;
    }

    /**
     * @return bool
     */
    public function getUsesTemporaryPassword(): bool
    {
        return $this->usesTemporaryPassword;
    }

    /**
     * @param bool $usesTemporaryPassword
     * @return static
     */
    public function setUsesTemporaryPassword(bool $usesTemporaryPassword): AbstractUserDTO
    {
        $this->usesTemporaryPassword = $usesTemporaryPassword;
        return $this;
    }
}
