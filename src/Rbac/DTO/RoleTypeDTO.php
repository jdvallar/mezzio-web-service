<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Rbac\DTO;


class RoleTypeDTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return RoleTypeDTO
     */
    public function setId(string $id): RoleTypeDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return RoleTypeDTO
     */
    public function setName(string $name): RoleTypeDTO
    {
        $this->name = $name;
        return $this;
    }
}
