<?php


namespace Vallarj\Mezzio\WebService\Rbac\DTO;


class RoleDTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $name;

    /** @var PermissionDTO[] */
    private $permissions;

    /** @var RoleTypeDTO|null */
    private $roleType;

    /** @var bool */
    private $default;

    /** @var bool */
    private $inUse;

    /**
     * RoleDTO constructor.
     */
    public function __construct()
    {
        $this->permissions = [];
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return RoleDTO
     */
    public function setId(string $id): RoleDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     * @return RoleDTO
     */
    public function setName(string $name): RoleDTO
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return PermissionDTO[]
     */
    public function getPermissions(): array
    {
        return $this->permissions;
    }

    /**
     * @param PermissionDTO[] $permissions
     * @return RoleDTO
     */
    public function setPermissions(array $permissions): RoleDTO
    {
        $this->permissions = $permissions;
        return $this;
    }

    /**
     * @param PermissionDTO $permission
     * @return RoleDTO
     */
    public function addPermission(PermissionDTO $permission): RoleDTO
    {
        $this->permissions[] = $permission;
        return $this;
    }

    /**
     * @return bool
     */
    public function getDefault(): bool
    {
        return $this->default;
    }

    /**
     * @param bool $default
     * @return RoleDTO
     */
    public function setDefault(bool $default): RoleDTO
    {
        $this->default = $default;
        return $this;
    }

    /**
     * @return RoleTypeDTO|null
     */
    public function getRoleType(): ?RoleTypeDTO
    {
        return $this->roleType;
    }

    /**
     * @param RoleTypeDTO|null $roleType
     * @return RoleDTO
     */
    public function setRoleType(?RoleTypeDTO $roleType): RoleDTO
    {
        $this->roleType = $roleType;
        return $this;
    }

    /**
     * @return bool
     */
    public function getInUse(): bool
    {
        return $this->inUse;
    }

    /**
     * @param bool $inUse
     * @return RoleDTO
     */
    public function setInUse(bool $inUse): RoleDTO
    {
        $this->inUse = $inUse;
        return $this;
    }
}
