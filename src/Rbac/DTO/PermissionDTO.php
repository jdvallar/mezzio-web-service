<?php


namespace Vallarj\Mezzio\WebService\Rbac\DTO;


class PermissionDTO
{
    /** @var string */
    private $id;

    /** @var string */
    private $description;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @param string $id
     * @return PermissionDTO
     */
    public function setId(string $id): PermissionDTO
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return PermissionDTO
     */
    public function setDescription(string $description): PermissionDTO
    {
        $this->description = $description;
        return $this;
    }
}
