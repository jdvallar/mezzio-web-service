<?php


namespace Vallarj\Mezzio\WebService\Rbac\Exception;


use Vallarj\Mezzio\WebService\Exception\WebServiceException;

class RoleAlreadyExistsException extends WebServiceException
{
}
