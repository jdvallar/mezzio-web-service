<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Rbac\Schema;


use Vallarj\Mezzio\WebService\Rbac\DTO\RoleTypeDTO;
use Vallarj\JsonApi\Exception\InvalidSpecificationException;
use Vallarj\JsonApi\Schema\AbstractResourceSchema;
use Vallarj\JsonApi\Schema\Attribute;

class RoleTypeSchema extends AbstractResourceSchema
{
    /** @var string */
    protected $mappingClass = RoleTypeDTO::class;

    /** @var string */
    protected $resourceType = 'rbac-role-types';

    /**
     * RoleTypeSchema constructor.
     *
     * @throws InvalidSpecificationException
     */
    public function __construct()
    {
        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'name',
                'access' => [
                    Attribute::GET
                ],
            ],
        ]);
    }
}
