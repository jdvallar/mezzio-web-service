<?php


namespace Vallarj\Mezzio\WebService\Rbac\Schema;


use Vallarj\Mezzio\WebService\Rbac\DTO\PermissionDTO;
use Vallarj\JsonApi\Exception\InvalidSpecificationException;
use Vallarj\JsonApi\Schema\AbstractResourceSchema;
use Vallarj\JsonApi\Schema\Attribute;

class PermissionSchema extends AbstractResourceSchema
{
    /** @var string */
    protected $mappingClass = PermissionDTO::class;

    /** @var string */
    protected $resourceType = 'rbac-permissions';

    /**
     * PermissionSchema constructor.
     *
     * @throws InvalidSpecificationException
     */
    public function __construct()
    {
        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'description',
                'access' => [
                    Attribute::GET
                ],
            ],
        ]);
    }
}
