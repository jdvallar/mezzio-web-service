<?php


namespace Vallarj\Mezzio\WebService\Rbac\Schema;


use Laminas\Validator\Regex;
use Vallarj\JsonApi\Exception\InvalidSpecificationException;
use Vallarj\JsonApi\Schema\AbstractResourceSchema;
use Vallarj\JsonApi\Schema\Attribute;
use Vallarj\JsonApi\Schema\ToManyRelationship;

abstract class AbstractUserSchema extends AbstractResourceSchema
{
    /**
     * AbstractUserSchema constructor.
     *
     * @throws InvalidSpecificationException
     */
    public function __construct()
    {
        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'username',
                'access' => [
                    Attribute::GET
                ]
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'oldPassword',
                'access' => [
                    Attribute::PATCH
                ],
            ]
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'password',
                'required' => true,
                'access' => [
                    Attribute::PATCH
                ],
                'validators' => [
                    [
                        'name' => Regex::class,
                        'options' => [
                            'pattern' => '/^(?=.*\d)(?=.*[A-Z])(?!.*[^a-zA-Z0-9@#$^+=])(.{8,15})$/',
                            'messages' => [
                                Regex::NOT_MATCH => 'Password must be 8-15 characters and must contain at least one '.
                                    'digit and one capital letter.'
                            ],
                        ],
                    ],
                ],
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'firstName',
                'access' => [
                    Attribute::GET
                ],
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'middleName',
                'access' => [
                    Attribute::GET
                ],
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'lastName',
                'access' => [
                    Attribute::GET
                ],
            ],
        ]);

        $this->addRelationship([
            'type' => ToManyRelationship::class,
            'options' => [
                'key' => 'roles',
                'access' => [
                    ToManyRelationship::GET
                ],
                'expects' => [
                    RoleSchema::class
                ],
            ],
        ]);
    }
}
