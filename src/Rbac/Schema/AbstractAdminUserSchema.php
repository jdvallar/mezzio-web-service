<?php


namespace Vallarj\Mezzio\WebService\Rbac\Schema;


use Laminas\Validator\Regex;
use Laminas\Validator\StringLength;
use Vallarj\JsonApi\Exception\InvalidSpecificationException;
use Vallarj\JsonApi\Schema\AbstractResourceSchema;
use Vallarj\JsonApi\Schema\Attribute;
use Vallarj\JsonApi\Schema\ToManyRelationship;

abstract class AbstractAdminUserSchema extends AbstractResourceSchema
{
    /**
     * AbstractAdminUserSchema constructor.
     *
     * @throws InvalidSpecificationException
     */
    public function __construct()
    {
        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'username',
                'required' => true,
                'access' => [
                    Attribute::GET,
                    Attribute::POST
                ],
                'validators' => $this->getUsernameValidators()
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'resetPassword',
                'access' => [
                    Attribute::PATCH
                ],
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'firstName',
                'required' => true,
                'access' => [
                    Attribute::GET,
                    Attribute::POST,
                    Attribute::PATCH
                ],
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'max' => 150
                        ],
                    ],
                ]
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'middleName',
                'access' => [
                    Attribute::GET,
                    Attribute::POST,
                    Attribute::PATCH
                ],
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'max' => 150
                        ],
                    ],
                ]
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'lastName',
                'required' => true,
                'access' => [
                    Attribute::GET,
                    Attribute::POST,
                    Attribute::PATCH
                ],
                'validators' => [
                    [
                        'name' => StringLength::class,
                        'options' => [
                            'max' => 150
                        ],
                    ],
                ]
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'active',
                'access' => [
                    Attribute::GET,
                    Attribute::PATCH
                ],
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'temporaryPassword',
                'access' => [
                    Attribute::GET
                ],
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'usesTemporaryPassword',
                'access' => [
                    Attribute::GET
                ],
            ],
        ]);

        $this->addRelationship([
            'type' => ToManyRelationship::class,
            'options' => [
                'key' => 'roles',
                'access' => [
                    ToManyRelationship::GET,
                    ToManyRelationship::POST,
                    ToManyRelationship::PATCH
                ],
                'expects' => [
                    RoleSchema::class
                ],
            ],
        ]);
    }

    /**
     * Returns the username validators.
     * Override this function to replace the default validators.
     *
     * @return array[]
     */
    public function getUsernameValidators(): array
    {
        return [
            [
                'name' => Regex::class,
                'options' => [
                    'pattern' => '/^(?:[a-zA-Z0-9@_.-])+$/',
                    'messages' => [
                        Regex::NOT_MATCH => 'Username must only contain letters, numbers or (@_.-).'
                    ],
                ],
            ],
            [
                'name' => StringLength::class,
                'options' => [
                    'min' => 5,
                    'max' => 50,
                    'messages' => [
                        StringLength::TOO_SHORT => 'Username must be %min%-%max% characters long',
                        StringLength::TOO_LONG  => 'Username must be %min%-%max% characters long',
                    ]
                ],
            ],
        ];
    }
}
