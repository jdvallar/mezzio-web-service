<?php


namespace Vallarj\Mezzio\WebService\Rbac\Schema;


use Vallarj\Mezzio\WebService\Rbac\DTO\RoleDTO;
use Vallarj\JsonApi\Exception\InvalidSpecificationException;
use Vallarj\JsonApi\Schema\Attribute;
use Vallarj\JsonApi\Schema\Meta;
use Vallarj\JsonApi\Schema\ToManyRelationship;
use Vallarj\JsonApi\Schema\AbstractResourceSchema;
use Vallarj\JsonApi\Schema\ToOneRelationship;

class RoleSchema extends AbstractResourceSchema
{
    /** @var string */
    protected $mappingClass = RoleDTO::class;

    /** @var string */
    protected $resourceType = 'rbac-roles';

    /**
     * RoleSchema constructor.
     *
     * @throws InvalidSpecificationException
     */
    public function __construct()
    {
        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'name',
                'required' => true,
                'access' => [
                    Attribute::GET,
                    Attribute::POST,
                    Attribute::PATCH
                ],
            ],
        ]);

        $this->addAttribute([
            'type' => Attribute::class,
            'options' => [
                'key' => 'default',
                'access' => [
                    Attribute::GET
                ],
            ],
        ]);

        $this->addRelationship([
            'type' => ToManyRelationship::class,
            'options' => [
                'key' => 'permissions',
                'access' => [
                    ToManyRelationship::GET,
                    ToManyRelationship::POST,
                    ToManyRelationship::PATCH
                ],
                'expects' => [
                    PermissionSchema::class
                ],
            ],
        ]);

        $this->addRelationship([
            'type' => ToOneRelationship::class,
            'options' => [
                'key' => 'roleType',
                'access' => [
                    ToManyRelationship::GET,
                    ToManyRelationship::POST
                ],
                'expects' => [
                    RoleTypeSchema::class
                ],
            ],
        ]);

        $this->addMeta([
            'type' => Meta::class,
            'options' => [
                'key' => 'inUse'
            ]
        ]);
    }
}
