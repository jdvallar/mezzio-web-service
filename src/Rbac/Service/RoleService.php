<?php


namespace Vallarj\Mezzio\WebService\Rbac\Service;


use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Vallarj\Laminas\Rbac\Entity\Role;
use Vallarj\Laminas\Rbac\Repository\RoleRepositoryInterface;
use Vallarj\Mezzio\WebService\Rbac\DTO\RoleDTO;
use Vallarj\Mezzio\WebService\Rbac\Exception\ActionNotAllowedException;
use Vallarj\Mezzio\WebService\Rbac\Exception\RoleAlreadyExistsException;
use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\Exception\MapperNotRegisteredException;
use Vallarj\ObjectMapper\Exception\MaxDepthReachedException;

class RoleService implements RoleServiceInterface
{
    /** @var RoleRepositoryInterface */
    private $roleRepository;

    /** @var AutoMapperInterface */
    private $autoMapper;

    /** @var EntityManagerInterface */
    private $entityManager;

    /**
     * RoleService constructor.
     *
     * @param RoleRepositoryInterface $roleRepository
     * @param AutoMapperInterface $autoMapper
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(
        RoleRepositoryInterface $roleRepository,
        AutoMapperInterface $autoMapper,
        EntityManagerInterface $entityManager
    ) {
        $this->roleRepository = $roleRepository;
        $this->autoMapper = $autoMapper;
        $this->entityManager = $entityManager;
    }

    /**
     * @inheritDoc
     */
    public function fetchAllRoles(): array
    {
        $roles = $this->roleRepository->findAllRoles();

        $roleDTOs = [];
        foreach ($roles as $role) {
            $roleDTOs[] = $this->autoMapper->map($role, RoleDTO::class);
        }

        return $roleDTOs;
    }

    /**
     * @inheritDoc
     */
    public function fetchRole(string $id): RoleDTO
    {
        $role = $this->roleRepository->getRole($id);
        return $this->autoMapper->map($role, RoleDTO::class);
    }

    /**
     * @inheritDoc
     * @throws RoleAlreadyExistsException
     * @throws MapperNotRegisteredException
     * @throws MaxDepthReachedException
     */
    public function createRole(RoleDTO $roleDTO): RoleDTO
    {
        /** @var Role $role */
        $role = $this->autoMapper->map($roleDTO, Role::class);

        try {
            $this->entityManager->persist($role);
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new RoleAlreadyExistsException("Role with ID: {$roleDTO->getId()} already exists.");
        }

        return $this->autoMapper->map($role, RoleDTO::class);
    }

    /**
     * @inheritDoc
     * @throws ActionNotAllowedException
     */
    public function updateRole(string $id, RoleDTO $roleDTO, array $updatedProps): void
    {
        $role = $this->roleRepository->getRole($id);

        if ($role->getDefault() || $role->isInternal()) {
            throw new ActionNotAllowedException("Unable to update default role.");
        }

        $this->autoMapper->map($roleDTO, $role, $updatedProps);
        $this->entityManager->flush();
    }

    /**
     * @inheritDoc
     * @throws ActionNotAllowedException
     */
    public function deleteRole(string $id): void
    {
        $role = $this->roleRepository->getRole($id);

        if ($role->getDefault() || $role->isInternal()) {
            throw new ActionNotAllowedException("Unable to delete default role.");
        }

        $this->entityManager->remove($role);
        $this->entityManager->flush();
    }
}
