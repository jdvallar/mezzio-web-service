<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Rbac\Service;


use Vallarj\Laminas\Rbac\Repository\RoleTypeRepositoryInterface;
use Vallarj\Mezzio\WebService\Rbac\DTO\RoleTypeDTO;
use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;

class RoleTypeService implements RoleTypeServiceInterface
{
    /** @var RoleTypeRepositoryInterface */
    private $roleTypeRepository;

    /** @var AutoMapperInterface */
    private $autoMapper;

    /**
     * RoleTypeService constructor.
     *
     * @param RoleTypeRepositoryInterface $roleTypeRepository
     * @param AutoMapperInterface $autoMapper
     */
    public function __construct(RoleTypeRepositoryInterface $roleTypeRepository, AutoMapperInterface $autoMapper)
    {
        $this->roleTypeRepository = $roleTypeRepository;
        $this->autoMapper = $autoMapper;
    }

    /**
     * @inheritDoc
     */
    public function fetchAllRoleTypes(): array
    {
        $roleTypes = $this->roleTypeRepository->findAllRoleTypes();

        $roleTypeDTOs = [];
        foreach ($roleTypes as $roleType) {
            $roleTypeDTOs[] = $this->autoMapper->map($roleType, RoleTypeDTO::class, ['*']);
        }

        return $roleTypeDTOs;
    }

    /**
     * @inheritDoc
     */
    public function fetchRoleType(string $id): RoleTypeDTO
    {
        $roleType = $this->roleTypeRepository->getRoleType($id);
        return $this->autoMapper->map($roleType, RoleTypeDTO::class, ['*']);
    }
}
