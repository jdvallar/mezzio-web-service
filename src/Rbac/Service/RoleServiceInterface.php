<?php


namespace Vallarj\Mezzio\WebService\Rbac\Service;


use Vallarj\Mezzio\WebService\Rbac\DTO\RoleDTO;
use Vallarj\Mezzio\WebService\Rbac\Exception\RoleAlreadyExistsException;

interface RoleServiceInterface
{
    /**
     * Fetches all roles
     *
     * @return RoleDTO[]
     */
    public function fetchAllRoles(): array;

    /**
     * Fetches a role by ID
     *
     * @param string $id
     * @return RoleDTO
     */
    public function fetchRole(string $id): RoleDTO;

    /**
     * Creates a role
     *
     * @param RoleDTO $roleDTO
     * @return RoleDTO
     * @throws RoleAlreadyExistsException
     */
    public function createRole(RoleDTO $roleDTO): RoleDTO;

    /**
     * Updates a role
     *
     * @param string $id
     * @param RoleDTO $roleDTO
     * @param array $updatedProps
     * @return void
     */
    public function updateRole(string $id, RoleDTO $roleDTO, array $updatedProps): void;

    /**
     * Deletes a role
     *
     * @param string $id
     * @return void
     */
    public function deleteRole(string $id): void;
}
