<?php


namespace Vallarj\Mezzio\WebService\Rbac\Service;


use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\EntityManagerInterface;
use Vallarj\Laminas\Rbac\Entity\AbstractUser;
use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Laminas\Rbac\Exception\RbacUserNotFoundException;
use Vallarj\Laminas\Rbac\Repository\RbacUserRepositoryInterface;
use Vallarj\Laminas\Rbac\Utilities\Password\PasswordInterface;
use Vallarj\Mezzio\WebService\Exception\InvalidCurrentPasswordException;
use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\Exception\MapperNotRegisteredException;
use Vallarj\ObjectMapper\Exception\MaxDepthReachedException;
use Vallarj\Mezzio\WebService\Exception\UserWithUsernameAlreadyExistsException;
use Vallarj\Mezzio\WebService\Rbac\DTO\AbstractUserDTO;
use Ory\Hydra\Client\Api\AdminApi;
use Ory\Hydra\Client\ApiException;

class UserService implements UserServiceInterface
{
    /** @var RbacUserRepositoryInterface */
    private $userRepository;

    /** @var AutoMapperInterface */
    private $autoMapper;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var PasswordInterface */
    private $passwordService;

    /** @var AdminApi */
    private $hydraAdmin;

    /** @var string */
    private $userDTOClassName;

    /** @var string */
    private $userEntityClassName;

    /** @var int */
    private $usersPageCount;

    /**
     * UserService constructor.
     *
     * @param RbacUserRepositoryInterface $userRepository
     * @param AutoMapperInterface $autoMapper
     * @param EntityManagerInterface $entityManager
     * @param PasswordInterface $passwordService
     * @param AdminApi $hydraAdmin
     * @param string $userDTOClassName
     * @param string $userEntityClassName
     * @param int $usersPageCount
     */
    public function __construct(
        RbacUserRepositoryInterface $userRepository,
        AutoMapperInterface $autoMapper,
        EntityManagerInterface $entityManager,
        PasswordInterface $passwordService,
        AdminApi $hydraAdmin,
        string $userDTOClassName,
        string $userEntityClassName,
        int $usersPageCount
    ) {
        $this->userRepository = $userRepository;
        $this->autoMapper = $autoMapper;
        $this->entityManager = $entityManager;
        $this->passwordService = $passwordService;
        $this->hydraAdmin = $hydraAdmin;
        $this->userDTOClassName = $userDTOClassName;
        $this->userEntityClassName = $userEntityClassName;
        $this->usersPageCount = $usersPageCount;
    }

    /**
     * @inheritDoc
     * @throws MapperNotRegisteredException
     * @throws MaxDepthReachedException
     */
    public function fetchAllUsers(RbacUser $actor): array
    {
        $users = $this->userRepository->findAllUsers();

        $userDTOs = [];
        foreach ($users as $user) {
            $userDTOs[] = $this->autoMapper->map($user, $this->userDTOClassName);
        }

        return $userDTOs;
    }

    /**
     * @inheritDoc
     * @throws MapperNotRegisteredException
     * @throws MaxDepthReachedException
     */
    public function fetchUsers(RbacUser $actor, ?string $searchQuery, ?string $lastId, ?int $length): array
    {
        if (is_null($length)) {
            $length = $this->usersPageCount;
        }

        $users = $this->userRepository->findUsers($length, $searchQuery, $lastId);

        $userDTOs = [];
        foreach ($users as $user) {
            $userDTOs[] = $this->autoMapper->map($user, $this->userDTOClassName);
        }

        return $userDTOs;
    }

    /**
     * @inheritDoc
     * @throws MapperNotRegisteredException
     * @throws MaxDepthReachedException
     */
    public function fetchUser(RbacUser $actor, string $id): ?AbstractUserDTO
    {
        try {
            $user = $this->userRepository->getUser($id);
        } catch (RbacUserNotFoundException $e) {
            return null;
        }

        return $this->autoMapper->map($user, $this->userDTOClassName);
    }

    /**
     * @inheritDoc
     * @throws MapperNotRegisteredException
     * @throws MaxDepthReachedException
     */
    public function createUser(RbacUser $actor, AbstractUserDTO $userDTO): AbstractUserDTO
    {
        /** @var AbstractUser $user */
        $user = $this->autoMapper->map($userDTO, $this->userEntityClassName);
        $this->entityManager->persist($user);
        try {
            $this->entityManager->flush();
        } catch (UniqueConstraintViolationException $exception) {
            throw new UserWithUsernameAlreadyExistsException(
                "User with username: {$userDTO->getUsername()} already exists."
            );
        }

        // Fetch user
        return $this->autoMapper->map($user, new $userDTO);
    }

    /**
     * @inheritDoc
     * @throws ApiException
     * @throws InvalidCurrentPasswordException
     * @throws MapperNotRegisteredException
     * @throws MaxDepthReachedException
     */
    public function updateUser(RbacUser $actor, string $id, AbstractUserDTO $userDTO, array $updatedProps): void
    {
        $user = $this->userRepository->getUser($id);

        // If changing password and incorrect current password
        if (
            in_array('password', $updatedProps)
            && !$user->authenticate($userDTO->getOldPassword() ?? "", $this->passwordService)
        ) {
            throw new InvalidCurrentPasswordException("Old password is incorrect.");
        }

        $rolesChanged = false;
        if (in_array('roles', $updatedProps)) {
            $currentRoles = [];
            foreach ($user->getRoles() as $role) {
                $currentRoles[] = $role->getId();
            }

            $newRoles = [];
            foreach ($userDTO->getRoles() as $newRole) {
                $newRoles[] = $newRole->getId();
            }

            $rolesChanged = (array_diff($currentRoles, $newRoles) !== array_diff($newRoles, $currentRoles));
        }

        $this->autoMapper->map($userDTO, $user, $updatedProps);

        if (
            (in_array('resetPassword', $updatedProps) && $userDTO->getResetPassword())
            || (in_array('active', $updatedProps) && !$userDTO->getActive())
            || (in_array('password', $updatedProps))
            || $rolesChanged
        ) {
            // Log user out if resetting password or deactivating account or roles have changed
            $this->hydraAdmin->revokeAuthenticationSession($user->getId());

            try {
                $this->hydraAdmin->revokeConsentSessions($user->getId(), null, "true");
            } catch (ApiException $e) {
                if ($e->getCode() != 404) {
                    throw $e;
                }
            }
        }

        $this->entityManager->flush();
    }
}
