<?php


namespace Vallarj\Mezzio\WebService\Rbac\Service;


use Vallarj\Mezzio\WebService\Rbac\DTO\PermissionDTO;

interface PermissionServiceInterface
{
    /**
     * Fetches all permissions
     *
     * @return PermissionDTO[]
     */
    public function fetchAllPermissions(): array;

    /**
     * Fetches a permission by ID
     *
     * @param string $id
     * @return PermissionDTO
     */
    public function fetchPermission(string $id): PermissionDTO;
}
