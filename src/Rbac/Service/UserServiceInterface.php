<?php


namespace Vallarj\Mezzio\WebService\Rbac\Service;


use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Mezzio\WebService\Exception\InvalidCurrentPasswordException;
use Vallarj\Mezzio\WebService\Exception\UserWithUsernameAlreadyExistsException;
use Vallarj\Mezzio\WebService\Rbac\DTO\AbstractUserDTO;

interface UserServiceInterface
{
    /**
     * Fetch all users
     *
     * @param RbacUser $actor
     * @return AbstractUserDTO[]
     */
    public function fetchAllUsers(RbacUser $actor): array;

    /**
     * Fetches users according to specified search parameters
     *
     * @param RbacUser $actor
     * @param string|null $searchQuery
     * @param string|null $lastId
     * @param int|null $length
     * @return array
     */
    public function fetchUsers(RbacUser $actor, ?string $searchQuery, ?string $lastId, ?int $length): array;

    /**
     * Fetches a user by ID
     *
     * @param RbacUser $actor
     * @param string $id
     * @return AbstractUserDTO|null
     */
    public function fetchUser(RbacUser $actor, string $id): ?AbstractUserDTO;

    /**
     * Creates a user
     *
     * @param RbacUser $actor
     * @param AbstractUserDTO $userDTO
     * @return AbstractUserDTO
     * @throws UserWithUsernameAlreadyExistsException
     */
    public function createUser(RbacUser $actor, AbstractUserDTO $userDTO): AbstractUserDTO;

    /**
     * Updates a user
     *
     * @param RbacUser $actor
     * @param string $id
     * @param AbstractUserDTO $userDTO
     * @param string[] $updatedProps
     * @return void
     * @throws InvalidCurrentPasswordException
     */
    public function updateUser(RbacUser $actor, string $id, AbstractUserDTO $userDTO, array $updatedProps): void;
}
