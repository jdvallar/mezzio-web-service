<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Rbac\Service;


use Vallarj\Mezzio\WebService\Rbac\DTO\RoleTypeDTO;

interface RoleTypeServiceInterface
{
    /**
     * Fetches all role types
     *
     * @return RoleTypeDTO[]
     */
    public function fetchAllRoleTypes(): array;

    /**
     * Fetches a role type by ID
     *
     * @param string $id
     * @return RoleTypeDTO
     */
    public function fetchRoleType(string $id): RoleTypeDTO;
}
