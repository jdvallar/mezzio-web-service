<?php


namespace Vallarj\Mezzio\WebService\Rbac\Service;


use Vallarj\Laminas\Rbac\Repository\PermissionRepositoryInterface;
use Vallarj\Mezzio\WebService\Rbac\DTO\PermissionDTO;
use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;

class PermissionService implements PermissionServiceInterface
{
    /** @var PermissionRepositoryInterface */
    private $permissionRepository;

    /** @var AutoMapperInterface */
    private $autoMapper;

    /**
     * PermissionService constructor.
     *
     * @param PermissionRepositoryInterface $permissionRepository
     * @param AutoMapperInterface $autoMapper
     */
    public function __construct(PermissionRepositoryInterface $permissionRepository, AutoMapperInterface $autoMapper)
    {
        $this->permissionRepository = $permissionRepository;
        $this->autoMapper = $autoMapper;
    }

    /**
     * @inheritDoc
     */
    public function fetchAllPermissions(): array
    {
        $permissions = $this->permissionRepository->findAllPermissions();

        $permissionDTOs = [];
        foreach ($permissions as $permission) {
            $permissionDTOs[] = $this->autoMapper->map($permission, PermissionDTO::class);
        }

        return $permissionDTOs;
    }

    /**
     * @inheritDoc
     */
    public function fetchPermission(string $id): PermissionDTO
    {
        $permission = $this->permissionRepository->getPermission($id);
        return $this->autoMapper->map($permission, PermissionDTO::class);
    }
}
