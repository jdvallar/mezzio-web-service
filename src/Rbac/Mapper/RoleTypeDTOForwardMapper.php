<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Entity\RoleType;
use Vallarj\Mezzio\WebService\Rbac\DTO\RoleTypeDTO;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;

class RoleTypeDTOForwardMapper extends AbstractObjectMapper
{
    /** @var string */
    protected $sourceClass = RoleType::class;

    /** @var string */
    protected $targetClass = RoleTypeDTO::class;

    /**
     * RoleTypeDTOForwardMapper constructor.
     */
    public function __construct()
    {
        $this->mapAttribute('name');
    }
}
