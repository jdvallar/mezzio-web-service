<?php


namespace Vallarj\Mezzio\WebService\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Entity\Role;
use Vallarj\Laminas\Rbac\Repository\RbacUserRepositoryInterface;
use Vallarj\Mezzio\WebService\Rbac\DTO\PermissionDTO;
use Vallarj\Mezzio\WebService\Rbac\DTO\RoleDTO;
use Vallarj\Mezzio\WebService\Rbac\DTO\RoleTypeDTO;
use Vallarj\ObjectMapper\Exception\InvalidArgumentException;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\ObjectMapper\Mapper\Strategy\MappingStrategy;

class RoleDTOForwardMapper extends AbstractObjectMapper
{
    /** @var string */
    protected $sourceClass = Role::class;

    /** @var string */
    protected $targetClass = RoleDTO::class;

    /** @var RbacUserRepositoryInterface */
    private $userRepository;

    /**
     * RoleDTOForwardMapper constructor.
     *
     * @param RbacUserRepositoryInterface $userRepository
     * @throws InvalidArgumentException
     */
    public function __construct(RbacUserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->mapAttribute('name')
            ->mapAttribute('default')
            ->mapToManyAssociation('permissions', PermissionDTO::class)
            ->mapToOneAssociation('roleType', RoleTypeDTO::class);

        // Meta properties
        $this->mapAttribute('inUse', true, $this->getInUseGetterSetterStrategy());
    }

    /**
     * Returns the inUse property getter-setter strategy
     *
     * @return MappingStrategy
     */
    private function getInUseGetterSetterStrategy(): MappingStrategy
    {
        return new class($this->userRepository) implements MappingStrategy
        {
            /** @var RbacUserRepositoryInterface */
            private $userRepository;

            /** @var Role[] */
            private $usedRoles;

            /**
             * MappingStrategy constructor.
             *
             * @param RbacUserRepositoryInterface $userRepository
             */
            public function __construct(RbacUserRepositoryInterface $userRepository)
            {
                $this->userRepository = $userRepository;
            }

            /**
             * @inheritDoc
             * @param Role $source
             */
            public function getValue(string $propertyName, $source)
            {
                if (is_null($this->usedRoles)) {
                    $roles = $this->userRepository->findAllUsedRoles();
                    foreach ($roles as $role) {
                        $this->usedRoles[$role->getId()] = true;
                    }
                }

                return isset($this->usedRoles[$source->getId()]);
            }

            /**
             * @inheritDoc
             * @param RoleDTO $target
             */
            public function setValue(string $propertyName, $target, $value): void
            {
                $target->setInUse($value);
            }
        };
    }
}
