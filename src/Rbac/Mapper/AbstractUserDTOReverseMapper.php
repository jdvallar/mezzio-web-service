<?php


namespace Vallarj\Mezzio\WebService\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Utilities\Password\Bcrypt;
use Vallarj\Laminas\Rbac\Utilities\Password\PasswordInterface;
use Vallarj\Laminas\Rbac\Entity\AbstractUser;
use Vallarj\Laminas\Rbac\Entity\Role;
use Vallarj\Laminas\Rbac\Repository\RbacUserRepositoryInterface;
use Vallarj\Mezzio\WebService\Rbac\DTO\AbstractUserDTO;
use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\AutoMapper\Context;
use Vallarj\ObjectMapper\Exception\InvalidArgumentException;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\ObjectMapper\Mapper\Strategy\GetterSetterMappingStrategy;
use Vallarj\ObjectMapper\Mapper\Strategy\MappingStrategy;

abstract class AbstractUserDTOReverseMapper extends AbstractObjectMapper
{
    /** @var RbacUserRepositoryInterface */
    private $userRepository;

    /** @var PasswordInterface */
    private $passwordService;

    /**
     * AbstractUserDTOReverseMapper constructor.
     *
     * @param RbacUserRepositoryInterface $userRepository
     * @throws InvalidArgumentException
     */
    public function __construct(RbacUserRepositoryInterface $userRepository)
    {
        $this->userRepository = $userRepository;

        $this->setIdentifier('id', false)
            ->mapAttribute('username', false)
            ->mapAttribute('password', false, $this->getPasswordGetterSetterStrategy())
            ->mapAttribute('firstName', false)
            ->mapAttribute('middleName')
            ->mapAttribute('lastName', false)
            ->mapAttribute('active', false)
            ->mapAttribute('resetPassword', false, $this->getResetPasswordGetterSetterStrategy())
            ->mapToManyAssociation('roles', Role::class, true, new GetterSetterMappingStrategy());
    }

    /**
     * Gets the password service
     *
     * @return PasswordInterface
     */
    public function getPasswordService(): PasswordInterface
    {
        if (!$this->passwordService) {
            $this->passwordService = new Bcrypt();
        }

        return $this->passwordService;
    }

    /**
     * Sets the default password service
     *
     * @param PasswordInterface $passwordService
     * @return void
     */
    public function setPasswordService(PasswordInterface $passwordService): void
    {
        $this->passwordService = $passwordService;
    }

    /**
     * @inheritDoc
     * @param AbstractUserDTO $source
     */
    public function createTargetObject(AutoMapperInterface $autoMapper, $source, string $targetClass, Context $context)
    {
        // Create a temporary password
        $temporaryPassword = substr(base64_encode(MD5(microtime())), 0, 10);

        return new $targetClass(
            $source->getUsername(),
            $temporaryPassword,
            $source->getFirstName(),
            $source->getLastName(),
            true,
            $this->getPasswordService()
        );
    }

    /**
     * @inheritDoc
     */
    public function resolveTargetOnAssociation($source, string $targetClass, Context $context)
    {
        $identifier = $this->getSourceIdentifier($source);
        return $this->userRepository->getUser($identifier);
    }

    /**
     * Returns the password property getter-setter strategy
     *
     * @return MappingStrategy
     */
    private function getPasswordGetterSetterStrategy(): MappingStrategy
    {
        return new class($this->getPasswordService()) extends GetterSetterMappingStrategy
        {
            /** @var PasswordInterface */
            private $passwordService;

            /**
             * Password getter-setter strategy constructor.
             *
             * @param PasswordInterface $passwordService
             */
            public function __construct(PasswordInterface $passwordService)
            {
                $this->passwordService = $passwordService;
            }

            /**
             * @inheritDoc
             * @param AbstractUser $target
             */
            public function setValue(string $propertyName, $target, $value): void
            {
                $target->changePassword($value, $this->passwordService);
            }
        };
    }

    /**
     * Returns the reset password property getter-setter strategy
     *
     * @return MappingStrategy
     */
    private function getResetPasswordGetterSetterStrategy(): MappingStrategy
    {
        return new class($this->getPasswordService()) extends GetterSetterMappingStrategy
        {
            /** @var PasswordInterface */
            private $passwordService;

            /**
             * Password getter-setter strategy constructor.
             *
             * @param PasswordInterface $passwordService
             */
            public function __construct(PasswordInterface $passwordService)
            {
                $this->passwordService = $passwordService;
            }

            /**
             * @inheritDoc
             * @param AbstractUser $target
             */
            public function setValue(string $propertyName, $target, $value): void
            {
                // Create a temporary password
                $temporaryPassword = substr(base64_encode(MD5(microtime())), 0, 10);

                if ($value === true) {
                    $target->setTemporaryPassword($temporaryPassword, $this->passwordService);
                }
            }
        };
    }
}
