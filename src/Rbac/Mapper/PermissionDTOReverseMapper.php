<?php


namespace Vallarj\Mezzio\WebService\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Entity\Permission;
use Vallarj\Laminas\Rbac\Repository\PermissionRepositoryInterface;
use Vallarj\Mezzio\WebService\Rbac\DTO\PermissionDTO;
use Vallarj\Mezzio\WebService\Rbac\Exception\PermissionNotFoundException;
use Vallarj\ObjectMapper\AutoMapper\Context;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;

class PermissionDTOReverseMapper extends AbstractObjectMapper
{
    /** @var string */
    protected $sourceClass = PermissionDTO::class;

    /** @var string */
    protected $targetClass = Permission::class;

    /** @var PermissionRepositoryInterface */
    private $permissionRepository;

    /** @var Permission[] */
    private $permissions;

    /**
     * PermissionDTOReverseMapper constructor.
     *
     * @param PermissionRepositoryInterface $permissionRepository
     */
    public function __construct(PermissionRepositoryInterface $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
        $this->permissions = [];
    }

    /**
     * @inheritDoc
     */
    public function resolveTargetOnAssociation($source, string $targetClass, Context $context)
    {
        $identifier = $this->getSourceIdentifier($source);
        return $this->getPermission($identifier);
    }

    /**
     * Gets a permission by ID
     *
     * @param string $permissionId
     * @return Permission
     * @throws PermissionNotFoundException
     */
    private function getPermission(string $permissionId): Permission
    {
        if (empty($this->permissions)) {
            $permissions = $this->permissionRepository->findAllPermissions();
            foreach ($permissions as $permission) {
                $this->permissions[$permission->getId()] = $permission;
            }
        }

        $permission = $this->permissions[$permissionId] ?? null;
        if (!$permission) {
            throw new PermissionNotFoundException("Permission with ID: ${permissionId} not found.");
        }

        return $permission;
    }
}
