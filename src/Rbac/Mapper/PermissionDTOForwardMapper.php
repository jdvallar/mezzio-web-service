<?php


namespace Vallarj\Mezzio\WebService\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Entity\Permission;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\Mezzio\WebService\Rbac\DTO\PermissionDTO;

class PermissionDTOForwardMapper extends AbstractObjectMapper
{
    /** @var string */
    protected $sourceClass = Permission::class;

    /** @var string */
    protected $targetClass = PermissionDTO::class;

    /**
     * PermissionDTOForwardMapper constructor.
     */
    public function __construct()
    {
        $this->mapAttribute('description');
    }
}
