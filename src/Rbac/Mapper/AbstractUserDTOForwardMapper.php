<?php


namespace Vallarj\Mezzio\WebService\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Entity\AbstractUser;
use Vallarj\Mezzio\WebService\Rbac\DTO\AbstractUserDTO;
use Vallarj\Mezzio\WebService\Rbac\DTO\RoleDTO;
use Vallarj\ObjectMapper\Exception\InvalidArgumentException;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\ObjectMapper\Mapper\Strategy\MappingStrategy;

abstract class AbstractUserDTOForwardMapper extends AbstractObjectMapper
{
    /**
     * AbstractUserDTOForwardMapper constructor.
     *
     * @throws InvalidArgumentException
     */
    public function __construct()
    {
        $this->mapAttribute('username')
            ->mapAttribute('firstName')
            ->mapAttribute('middleName')
            ->mapAttribute('lastName')
            ->mapAttribute('active', true, $this->getActiveGetterStrategy())
            ->mapAttribute('temporaryPassword')
            ->mapAttribute('usesTemporaryPassword')
            ->mapToManyAssociation('roles', RoleDTO::class);
    }

    /**
     * Returns the active property getter-setter strategy
     *
     * @return MappingStrategy
     */
    private function getActiveGetterStrategy(): MappingStrategy
    {
        return new class implements MappingStrategy
        {
            /**
             * @inheritDoc
             * @param AbstractUser $source
             */
            public function getValue(string $propertyName, $source)
            {
                return $source->isActive();
            }

            /**
             * @inheritDoc
             * @param AbstractUserDTO $target
             */
            public function setValue(string $propertyName, $target, $value): void
            {
                $target->setActive($value);
            }
        };
    }
}
