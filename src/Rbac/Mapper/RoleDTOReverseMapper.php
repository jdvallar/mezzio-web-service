<?php


namespace Vallarj\Mezzio\WebService\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Entity\Permission;
use Vallarj\Laminas\Rbac\Entity\Role;
use Vallarj\Laminas\Rbac\Entity\RoleType;
use Vallarj\Laminas\Rbac\Repository\RoleRepositoryInterface;
use Vallarj\Mezzio\WebService\Rbac\DTO\RoleDTO;
use Vallarj\Mezzio\WebService\Rbac\Exception\RoleNotFoundException;
use Vallarj\ObjectMapper\AutoMapper\AutoMapperInterface;
use Vallarj\ObjectMapper\AutoMapper\Context;
use Vallarj\ObjectMapper\Exception\InvalidArgumentException;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;
use Vallarj\ObjectMapper\Mapper\Strategy\GetterSetterMappingStrategy;

class RoleDTOReverseMapper extends AbstractObjectMapper
{
    /** @var string */
    protected $sourceClass = RoleDTO::class;

    /** @var string */
    protected $targetClass = Role::class;

    /** @var RoleRepositoryInterface */
    private $roleRepository;

    /** @var Role[] */
    private $roles;

    /**
     * RoleDTOReverseMapper constructor.
     *
     * @param RoleRepositoryInterface $roleRepository
     * @throws InvalidArgumentException
     */
    public function __construct(RoleRepositoryInterface $roleRepository)
    {
        $this->roleRepository = $roleRepository;
        $this->roles = [];

        $this->setIdentifier('id', false)
            ->mapAttribute('name', false)
            ->mapToManyAssociation('permissions', Permission::class, true, new GetterSetterMappingStrategy())
            ->mapToOneAssociation('roleType', RoleType::class, false);
    }

    /**
     * @inheritDoc
     * @param RoleDTO $source
     */
    public function createTargetObject(AutoMapperInterface $autoMapper, $source, string $targetClass, Context $context)
    {
        return new Role(
            $source->getId(),
            $source->getName(),
            $autoMapper->mapWithContext(
                $source->getRoleType(),
                RoleType::class,
                $context,
                'roleType'
            ),
            false,
            false
        );
    }

    /**
     * @inheritDoc
     * @throws RoleNotFoundException
     */
    public function resolveTargetOnAssociation($source, string $targetClass, Context $context)
    {
        $identifier = $this->getSourceIdentifier($source);
        return $this->getRole($identifier);
    }

    /**
     * Gets a role by ID
     *
     * @param string $roleId
     * @return Role
     * @throws RoleNotFoundException
     */
    private function getRole(string $roleId): Role
    {
        if (empty($this->roles)) {
            $roles = $this->roleRepository->findAllRoles();
            foreach ($roles as $role) {
                $this->roles[$role->getId()] = $role;
            }
        }

        $role = $this->roles[$roleId] ?? null;
        if (!$role) {
            throw new RoleNotFoundException("Role with ID: ${roleId} not found.");
        }

        return $role;
    }
}
