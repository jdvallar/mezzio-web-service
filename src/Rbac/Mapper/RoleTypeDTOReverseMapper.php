<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Entity\RoleType;
use Vallarj\Laminas\Rbac\Repository\RoleTypeRepositoryInterface;
use Vallarj\Mezzio\WebService\Rbac\DTO\RoleTypeDTO;
use Vallarj\ObjectMapper\AutoMapper\Context;
use Vallarj\ObjectMapper\Mapper\AbstractObjectMapper;

class RoleTypeDTOReverseMapper extends AbstractObjectMapper
{
    /** @var string */
    protected $sourceClass = RoleTypeDTO::class;

    /** @var string */
    protected $targetClass = RoleType::class;

    /** @var RoleTypeRepositoryInterface */
    private $roleTypeRepository;

    /**
     * RoleTypeDTOReverseMapper constructor.
     *
     * @param RoleTypeRepositoryInterface $roleTypeRepository
     */
    public function __construct(RoleTypeRepositoryInterface $roleTypeRepository)
    {
        $this->roleTypeRepository = $roleTypeRepository;
    }

    /**
     * @inheritDoc
     */
    public function resolveTargetOnAssociation($source, string $targetClass, Context $context)
    {
        $identifier = $this->getSourceIdentifier($source);
        return $this->roleTypeRepository->getRoleType($identifier);
    }
}
