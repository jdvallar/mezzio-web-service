<?php


namespace Vallarj\Mezzio\WebService\Router;


use Mezzio\Application;

class JsonApiRouter
{
    /** @var Application */
    private $application;

    /**
     * JsonApiRouter constructor.
     *
     * @param Application $application
     */
    public function __construct(Application $application)
    {
        $this->application = $application;
    }

    /**
     * Registers a route in the application instance
     *
     * @param string $basePath
     * @param string $handler
     * @param string $name
     * @param bool $autoAppendId
     * @return void
     */
    public function route(string $basePath, string $handler, string $name, bool $autoAppendId = true): void
    {
        if ($autoAppendId && $basePath !== '/') {
            $basePath = rtrim($basePath, '/') . "[/{id}]";
        }

        $this->application->route(
            $basePath,
            $handler,
            ['GET', 'POST', 'PATCH', 'DELETE'],
            $name
        );
    }
}
