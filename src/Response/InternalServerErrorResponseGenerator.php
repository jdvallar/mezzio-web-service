<?php


namespace Vallarj\Mezzio\WebService\Response;


use Vallarj\Mezzio\WebService\Exception\UninitializedErrorLogException;
use Vallarj\Mezzio\WebService\Service\LoggerServiceInterface;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Vallarj\JsonApi\Error\Error;
use Vallarj\JsonApi\Error\ErrorDocument;
use Throwable;

class InternalServerErrorResponseGenerator
{
    /** @var LoggerServiceInterface */
    private $loggerService;

    /**
     * InternalServerErrorResponseGenerator constructor.
     *
     * @param LoggerServiceInterface $loggerService
     */
    public function __construct(LoggerServiceInterface $loggerService)
    {
        $this->loggerService = $loggerService;
    }

    /**
     * Invokes the response generator
     *
     * @param Throwable $e
     * @param ServerRequestInterface $request
     * @param ResponseInterface $response
     * @return ResponseInterface
     * @throws UninitializedErrorLogException
     */
    public function __invoke(
        Throwable $e,
        ServerRequestInterface $request,
        ResponseInterface $response
    ) : ResponseInterface {
        $errDoc = new ErrorDocument(500);
        $error = new Error();

        if ($this->loggerService->hasErrorLog()) {
            $error->setId($this->loggerService->getErrorLogId());
        }

        $error->setTitle('Internal Server Error')
            ->setDetail('An unexpected error occurred.');
        $errDoc->addError($error);

        return new JsonApiResponse($errDoc);
    }
}
