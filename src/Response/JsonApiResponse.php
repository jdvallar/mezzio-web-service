<?php


namespace Vallarj\Mezzio\WebService\Response;


use JsonSerializable;
use Laminas\Diactoros\Response\JsonResponse;
use Vallarj\JsonApi\Error\ErrorDocument;

class JsonApiResponse extends JsonResponse
{
    private const CONTENT_TYPE = 'application/vnd.api+json';

    /**
     * JsonApiResponse constructor.
     *
     * @param array|object|JsonSerializable $data
     * @param int $status
     * @param array $headers
     */
    public function __construct($data, int $status = 200, array $headers = [])
    {
        if ($data instanceof ErrorDocument) {
            $status = $data->getStatusCode();
        }

        parent::__construct(
            $data,
            $status,
            array_merge([
                'Content-Type' => [self::CONTENT_TYPE]
            ], $headers),
            JSON_UNESCAPED_UNICODE | JSON_PRETTY_PRINT
        );
    }
}
