<?php


namespace Vallarj\Mezzio\WebService\Exception;


class InvalidCurrentPasswordException extends WebServiceException
{
}
