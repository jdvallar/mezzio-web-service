<?php


namespace Vallarj\Mezzio\WebService\Exception;


class MissingConfigurationException extends WebServiceException
{
}
