<?php


namespace Vallarj\Mezzio\WebService\Exception;


class UserWithUsernameAlreadyExistsException extends WebServiceException
{
}
