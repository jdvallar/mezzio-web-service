<?php


namespace Vallarj\Mezzio\WebService\Exception;


class InvalidResponseTypeException extends WebServiceException
{
}
