<?php


namespace Vallarj\Mezzio\WebService\Service;


use DateTime;
use Doctrine\DBAL\Exception;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\ORMException;
use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Mezzio\WebService\Entity\AccessLog;
use Vallarj\Mezzio\WebService\Entity\ErrorLog;
use Vallarj\Mezzio\WebService\Exception\AccessLogAlreadyCreatedException;
use Vallarj\Mezzio\WebService\Exception\UninitializedAccessLogException;
use Vallarj\Mezzio\WebService\Exception\UninitializedErrorLogException;
use Ramsey\Uuid\UuidFactoryInterface;
use Throwable;

class LoggerService implements LoggerServiceInterface
{
    /** @var AccessLog|null */
    private $currentAccessLog;

    /** @var ErrorLog|null */
    private $currentErrorLog;

    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var UuidFactoryInterface */
    private $uuidFactory;

    /**
     * LoggerService constructor.
     *
     * @param EntityManagerInterface $entityManager
     * @param UuidFactoryInterface $uuidFactory
     */
    public function __construct(EntityManagerInterface $entityManager, UuidFactoryInterface $uuidFactory)
    {
        $this->entityManager = $entityManager;
        $this->uuidFactory = $uuidFactory;
    }

    /**
     * @inheritDoc
     * @throws AccessLogAlreadyCreatedException
     */
    public function createAccessLog(
        DateTime $date,
        string $target,
        string $method,
        ?string $clientIp,
        ?string $remoteAddr,
        ?string $userAgent
    ): void {
        if (!is_null($this->currentAccessLog)) {
            throw new AccessLogAlreadyCreatedException("Access log entry already exists");
        }

        $this->currentAccessLog = new AccessLog($date, $target, $method);
        if ($clientIp) {
            $this->currentAccessLog->setClientIp(substr($clientIp, 0, 255));
        }
        if ($remoteAddr) {
            $this->currentAccessLog->setRemoteAddr(substr($remoteAddr, 0, 255));
        }
        if ($userAgent) {
            $this->currentAccessLog->setUserAgent(substr($userAgent, 0, 500));
        }
    }

    /**
     * @inheritDoc
     */
    public function hasAccessLog(): bool
    {
        return !is_null($this->currentAccessLog);
    }

    /**
     * @inheritDoc
     */
    public function attachErrorLog(Throwable $throwable)
    {
        $this->verifyAccessLogExists();
        $this->currentErrorLog = new ErrorLog($this->currentAccessLog, $throwable);
    }

    /**
     * @inheritDoc
     */
    public function hasErrorLog(): bool
    {
        return !is_null($this->currentErrorLog);
    }

    /**
     * @inheritDoc
     * @throws UninitializedErrorLogException
     */
    public function getErrorLogId(): string
    {
        if (is_null($this->currentErrorLog)) {
            throw new UninitializedErrorLogException("Error log entry has not been initialized.");
        }

        return $this->currentErrorLog->getOrderedId();
    }

    /**
     * @inheritDoc
     */
    public function setAccessLogRouteParameters(string $route, ?string $routeId): void
    {
        $this->verifyAccessLogExists();
        $this->currentAccessLog->setRoute($route)
            ->setRouteId($routeId);
    }

    /**
     * @inheritDoc
     */
    public function setAccessLogUser(?RbacUser $user): void
    {
        $this->verifyAccessLogExists();
        $this->currentAccessLog->setUser($user);
    }

    /**
     * @inheritDoc
     */
    public function setAccessLogBody($resource): void
    {
        $this->verifyAccessLogExists();

        $route = strtolower(trim($this->currentAccessLog->getRoute()));
        $routeId = strtolower(trim($this->currentAccessLog->getRouteId()));
        $method = strtoupper(trim($this->currentAccessLog->getMethod()));

        // Redact body if PATCH request to users/me endpoint
        if ($route == 'users' && $routeId == 'me' && $method == 'PATCH') {
            $stream = fopen('php://memory', 'r+');
            fwrite($stream, "REDACTED");
            rewind($stream);
            $this->currentAccessLog->setBody($stream);
        } else {
            $this->currentAccessLog->setBody($resource);
        }
    }

    /**
     * @inheritDoc
     * @throws Exception
     * @throws ORMException
     */
    public function commit(): void
    {
        $this->verifyAccessLogExists();
        if ($this->entityManager->isOpen()) {
            $user = $this->currentAccessLog->getUser();
            $this->entityManager->clear();
            if ($user) {
                $entityName = $this->entityManager->getClassMetadata(get_class($user))->getName();
                /** @var RbacUser $user */
                $user = $this->entityManager->getReference($entityName, $user->getId());
                $this->currentAccessLog->setUser($user);
            }

            $this->entityManager->persist($this->currentAccessLog);
            if ($this->currentErrorLog) {
                $this->entityManager->persist($this->currentErrorLog);
            }

            $this->entityManager->flush();
        } else {
            // Manually insert logs
            $connection = $this->entityManager->getConnection();
            $connection->beginTransaction();
            $accessLogId = $this->insertAccessLogManually();
            if ($this->currentErrorLog) {
                $this->insertErrorLogManually($accessLogId);
            }
            $connection->commit();
        }
    }

    /**
     * Inserts access log manually through DBAL
     *
     * @return string
     * @throws Exception
     * @throws \Exception
     */
    private function insertAccessLogManually(): string
    {
        $connection = $this->entityManager->getConnection();
        // Table name
        $tableName = $this->entityManager->getClassMetadata(AccessLog::class)->getTableName();
        $accessLogId = $this->uuidFactory->uuid1()->toString();
        $accessParams = [
            'id' => $accessLogId,
            'user_id' => $this->currentAccessLog->getUser()->getId(),
            'date' => $this->currentAccessLog->getDate(),
            'client_ip' => $this->currentAccessLog->getClientIp(),
            'remote_addr' => $this->currentAccessLog->getRemoteAddr(),
            'user_agent' => $this->currentAccessLog->getUserAgent(),
            'uri' => $this->currentAccessLog->getUri(),
            'route' => $this->currentAccessLog->getRoute(),
            'route_id' => $this->currentAccessLog->getRouteId(),
            'method' => $this->currentAccessLog->getMethod(),
            'body' => $this->currentAccessLog->getBody()
        ];
        $accessTypes = [
            'id' => 'uuid_binary_ordered_time',
            'user_id' => 'uuid_binary_ordered_time',
            'date' => 'datetime',
            'client_ip' => 'string',
            'remote_addr' => 'string',
            'user_agent' => 'string',
            'uri' => 'text',
            'route' => 'string',
            'route_id' => 'string',
            'method' => 'string',
            'body' => 'blob'
        ];

        $q = sprintf(
            "INSERT INTO %s(%s) VALUES(%s);",
            $tableName,
            implode(', ', array_keys($accessParams)),
            implode(', ', array_fill(0, count($accessParams), '?'))
        );

        $connection->executeStatement($q, array_values($accessParams), array_values($accessTypes));
        return $accessLogId;
    }

    /**
     * Inserts error log manually through DBAL
     *
     * @param string $accessLogId
     * @return void
     * @throws \Exception
     * @throws Exception
     */
    private function insertErrorLogManually(string $accessLogId): void
    {
        // Table name
        $tableName = $this->entityManager->getClassMetadata(ErrorLog::class)->getTableName();
        $errorParams = [
            'id' => $this->uuidFactory->uuid1()->toString(),
            'access_log_id' => $accessLogId,
            'code' => $this->currentErrorLog->getCode(),
            'file' => $this->currentErrorLog->getFile(),
            'line' => $this->currentErrorLog->getLine(),
            'message' => $this->currentErrorLog->getMessage(),
            'trace' => $this->currentErrorLog->getTrace(),
        ];
        $errorTypes = [
            'id' => 'uuid_binary_ordered_time',
            'access_log_id' => 'uuid_binary_ordered_time',
            'code' => 'integer',
            'file' => 'string',
            'line' => 'integer',
            'message' => 'text',
            'trace' => 'text'
        ];

        $q = sprintf(
            "INSERT INTO %s(%s) VALUES(%s);",
            $tableName,
            implode(', ', array_keys($errorParams)),
            implode(', ', array_fill(0, count($errorParams), '?'))
        );

        $connection = $this->entityManager->getConnection();
        $connection->executeStatement($q, array_values($errorParams), array_values($errorTypes));
    }

    /**
     * Throws UninitializedAccessLogException if access log has not been initialized
     *
     * @return void
     * @throws UninitializedAccessLogException
     */
    private function verifyAccessLogExists(): void
    {
        if (is_null($this->currentAccessLog)) {
            throw new UninitializedAccessLogException("Access log entry has not been initialized.");
        }
    }
}
