<?php


namespace Vallarj\Mezzio\WebService\Service;


use DateTime;
use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Vallarj\Mezzio\WebService\Exception\AccessLogAlreadyCreatedException;
use Vallarj\Mezzio\WebService\Exception\UninitializedAccessLogException;
use Vallarj\Mezzio\WebService\Exception\UninitializedErrorLogException;
use Throwable;

interface LoggerServiceInterface
{
    /**
     * Creates an access log entry.
     * Throws AccessLogAlreadyCreatedException if access log entry has already been created.
     *
     * @param DateTime $date
     * @param string $target
     * @param string $method
     * @param string|null $clientIp
     * @param string|null $remoteAddr
     * @param string|null $userAgent
     * @return void
     * @throws AccessLogAlreadyCreatedException
     */
    public function createAccessLog(
        DateTime $date,
        string $target,
        string $method,
        ?string $clientIp,
        ?string $remoteAddr,
        ?string $userAgent
    ): void;

    /**
     * Returns true if access log has been initialized
     *
     * @return bool
     */
    public function hasAccessLog(): bool;

    /**
     * Attaches an error log to the current access log entry
     * Throws UninitializedAccessLogException if access log entry has not been created
     *
     * @param Throwable $throwable
     * @return mixed
     * @throws UninitializedAccessLogException
     */
    public function attachErrorLog(Throwable $throwable);

    /**
     * Returns true if error log has been attached
     *
     * @return bool
     */
    public function hasErrorLog(): bool;

    /**
     * Returns the error log ID
     * Throws UninitializedErrorLogException if error log has not been attached
     *
     * @return string
     * @throws UninitializedErrorLogException
     */
    public function getErrorLogId(): string;

    /**
     * Sets the route parameters of the access log entry
     * Throws UninitializedAccessLogException if access log entry has not been created
     *
     * @param string $route
     * @param string|null $routeId
     * @return void
     * @throws UninitializedAccessLogException
     */
    public function setAccessLogRouteParameters(string $route, ?string $routeId): void;

    /**
     * Sets the user field of the access log entry
     * Throws UninitializedAccessLogException if access log entry has not been created
     *
     * @param RbacUser|null $user
     * @return void
     * @throws UninitializedAccessLogException
     */
    public function setAccessLogUser(?RbacUser $user): void;

    /**
     * Sets the body field of the access log entry
     * Throws UninitializedAccessLogException if access log entry has not been created
     *
     * @return void
     * @param mixed $resource
     * @throws UninitializedAccessLogException
     */
    public function setAccessLogBody($resource): void;

    /**
     * Commits the generated access and error logs
     * Throws UninitializedAccessLogException if access log entry has not been created
     *
     * @return void
     * @throws UninitializedAccessLogException
     */
    public function commit(): void;
}
