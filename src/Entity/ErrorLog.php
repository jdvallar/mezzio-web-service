<?php


namespace Vallarj\Mezzio\WebService\Entity;

use Doctrine\ORM\Mapping as ORM;
use Ramsey\Uuid\Uuid;
use Throwable;

/**
 * @ORM\Entity
 * @ORM\Table(name="_ws_log_error")
 */
class ErrorLog
{
    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="uuid_binary_ordered_time", unique=true)
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidOrderedTimeGenerator")
     */
    private $id;

    /**
     * @var AccessLog
     *
     * @ORM\ManyToOne(targetEntity="AccessLog")
     * @ORM\JoinColumn(name="access_log_id", referencedColumnName="id", nullable=false)
     */
    private $accessLog;

    /**
     * @var int
     *
     * @ORM\Column(name="code", type="integer")
     */
    private $code;

    /**
     * @var string
     *
     * @ORM\Column(name="file", type="string", length=500)
     */
    private $file;

    /**
     * @var int
     *
     * @ORM\Column(name="line", type="integer")
     */
    private $line;

    /**
     * @var string
     *
     * @ORM\Column(name="message", type="text")
     */
    private $message;

    /**
     * @var string
     *
     * @ORM\Column(name="trace", type="text")
     */
    private $trace;

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @return string
     */
    public function getOrderedId(): string
    {
        return strtoupper(bin2hex($this->id->getBytes()));
    }

    /**
     * ErrorLog constructor.
     *
     * @param AccessLog $accessLog
     * @param Throwable $throwable
     */
    public function __construct(AccessLog $accessLog, Throwable $throwable)
    {
        $this->accessLog = $accessLog;
        $this->code = $throwable->getCode();
        $this->file = $throwable->getFile();
        $this->line = $throwable->getLine();
        $this->message = $throwable->getMessage();
        $this->trace = self::getTraceAsString($throwable);
    }

    /**
     * @return AccessLog
     */
    public function getAccessLog(): AccessLog
    {
        return $this->accessLog;
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getFile(): string
    {
        return $this->file;
    }

    /**
     * @return int
     */
    public function getLine(): int
    {
        return $this->line;
    }

    /**
     * @return string
     */
    public function getMessage(): string
    {
        return $this->message;
    }

    /**
     * @return string
     */
    public function getTrace(): string
    {
        return $this->trace;
    }

    /**
     * Gets a throwable trace as string
     * Taken from https://www.php.net/manual/en/exception.gettraceasstring.php#114980
     *
     * @param Throwable $throwable
     * @param mixed|null $seen
     * @return string
     */
    private static function getTraceAsString(Throwable $throwable, $seen = null): string
    {
        $starter = $seen ? 'Caused by: ' : '';
        $result = array();
        if (!$seen) {
            $seen = array();
        }

        $trace  = $throwable->getTrace();
        $prev   = $throwable->getPrevious();
        $result[] = sprintf('%s%s: %s', $starter, get_class($throwable), $throwable->getMessage());
        $file = $throwable->getFile();
        $line = $throwable->getLine();
        while (true) {
            $current = "$file:$line";
            if (is_array($seen) && in_array($current, $seen)) {
                $result[] = sprintf(' ... %d more', count($trace)+1);
                break;
            }
            $result[] = sprintf(
                ' at %s%s%s(%s%s%s)',
                count($trace) && array_key_exists('class', $trace[0]) ? $trace[0]['class'] : '',
                count($trace)
                && array_key_exists('class', $trace[0])
                && array_key_exists('function', $trace[0]) ? '::' : '',
                count($trace)
                && array_key_exists('function', $trace[0]) ? $trace[0]['function'] : '(main)',
                $line === null ? $file : basename($file),
                $line === null ? '' : ':',
                $line === null ? '' : $line
            );

            if (is_array($seen)) {
                $seen[] = "$file:$line";
            }

            if (!count($trace)) {
                break;
            }

            $file = array_key_exists('file', $trace[0]) ? $trace[0]['file'] : 'Unknown Source';
            $line = array_key_exists('file', $trace[0])
            && array_key_exists('line', $trace[0])
            && $trace[0]['line'] ? $trace[0]['line'] : null;

            array_shift($trace);
        }
        $result = join("\n", $result);
        if ($prev) {
            $result  .= "\n" . self::getTraceAsString($prev, $seen);
        }

        return $result;
    }
}
