<?php


namespace Vallarj\Mezzio\WebService\Entity;


use DateTime;
use Doctrine\ORM\Mapping as ORM;
use Vallarj\Laminas\Rbac\Entity\RbacUser;
use Ramsey\Uuid\Uuid;

/**
 * @ORM\Entity
 * @ORM\Table(name="_ws_log_access")
 */
class AccessLog
{
    /**
     * @var Uuid
     *
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="CUSTOM")
     * @ORM\Column(name="id", type="uuid_binary_ordered_time", unique=true)
     * @ORM\CustomIdGenerator(class="Ramsey\Uuid\Doctrine\UuidOrderedTimeGenerator")
     */
    private $id;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="date", type="datetime")
     */
    private $date;

    /**
     * @var RbacUser|null
     *
     * @ORM\ManyToOne(targetEntity="Vallarj\Laminas\Rbac\Entity\AbstractUser")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var string|null
     *
     * @ORM\Column(name="client_ip", type="string", length=255, nullable=true)
     */
    private $clientIp;

    /**
     * @var string|null
     *
     * @ORM\Column(name="remote_addr", type="string", length=255, nullable=true)
     */
    private $remoteAddr;

    /**
     * @var string|null
     *
     * @ORM\Column(name="user_agent", type="string", length=500, nullable=true)
     */
    private $userAgent;

    /**
     * @var string
     *
     * @ORM\Column(name="uri", type="text")
     */
    private $uri;

    /**
     * @var string|null
     *
     * @ORM\Column(name="route", type="string", length=255, nullable=true)
     */
    private $route;

    /**
     * @var string|null
     *
     * @ORM\Column(name="route_id", type="string", length=255, nullable=true)
     */
    private $routeId;

    /**
     * @var string
     *
     * @ORM\Column(name="method", type="string", length=10)
     */
    private $method;

    /**
     * @var resource|null
     *
     * @ORM\Column(name="body", type="blob", nullable=true)
     */
    private $body;

    /**
     * AccessLog constructor.
     *
     * @param DateTime $date
     * @param string $uri
     * @param string $method
     */
    public function __construct(DateTime $date, string $uri, string $method)
    {
        $this->date = $date;
        $this->uri = $uri;
        $this->method = $method;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id->toString();
    }

    /**
     * @return DateTime
     */
    public function getDate(): DateTime
    {
        return $this->date;
    }

    /**
     * @param DateTime $date
     * @return AccessLog
     */
    public function setDate(DateTime $date): AccessLog
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return RbacUser|null
     */
    public function getUser(): ?RbacUser
    {
        return $this->user;
    }

    /**
     * @param RbacUser|null $user
     * @return AccessLog
     */
    public function setUser(?RbacUser $user): AccessLog
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getClientIp(): ?string
    {
        return $this->clientIp;
    }

    /**
     * @param string|null $clientIp
     * @return AccessLog
     */
    public function setClientIp(?string $clientIp): AccessLog
    {
        $this->clientIp = $clientIp;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRemoteAddr(): ?string
    {
        return $this->remoteAddr;
    }

    /**
     * @param string|null $remoteAddr
     * @return AccessLog
     */
    public function setRemoteAddr(?string $remoteAddr): AccessLog
    {
        $this->remoteAddr = $remoteAddr;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUserAgent(): ?string
    {
        return $this->userAgent;
    }

    /**
     * @param string|null $userAgent
     * @return AccessLog
     */
    public function setUserAgent(?string $userAgent): AccessLog
    {
        $this->userAgent = $userAgent;
        return $this;
    }

    /**
     * @return string
     */
    public function getUri(): string
    {
        return $this->uri;
    }

    /**
     * @param string $uri
     * @return AccessLog
     */
    public function setUri(string $uri): AccessLog
    {
        $this->uri = $uri;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRoute(): ?string
    {
        return $this->route;
    }

    /**
     * @param string|null $route
     * @return AccessLog
     */
    public function setRoute(?string $route): AccessLog
    {
        $this->route = $route;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getRouteId(): ?string
    {
        return $this->routeId;
    }

    /**
     * @param string|null $routeId
     * @return AccessLog
     */
    public function setRouteId(?string $routeId): AccessLog
    {
        $this->routeId = $routeId;
        return $this;
    }

    /**
     * @return string
     */
    public function getMethod(): string
    {
        return $this->method;
    }

    /**
     * @param string $method
     * @return AccessLog
     */
    public function setMethod(string $method): AccessLog
    {
        $this->method = $method;
        return $this;
    }

    /**
     * @return resource|null
     */
    public function getBody()
    {
        return $this->body;
    }

    /**
     * @param resource|null $body
     * @return AccessLog
     */
    public function setBody($body): AccessLog
    {
        $this->body = $body;
        return $this;
    }
}
