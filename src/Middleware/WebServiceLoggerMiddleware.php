<?php


namespace Vallarj\Mezzio\WebService\Middleware;


use DateTime;
use Doctrine\ORM\EntityManagerInterface;
use Vallarj\Mezzio\WebService\Entity\AccessLog;
use Vallarj\Mezzio\WebService\Entity\ErrorLog;
use Vallarj\Mezzio\WebService\Service\LoggerServiceInterface;
use Laminas\Diactoros\Response\EmptyResponse;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Throwable;

class WebServiceLoggerMiddleware implements MiddlewareInterface
{
    const ACCESS_LOG_ATTRIBUTE = 'web-service-access-log';

    /** @var LoggerServiceInterface */
    private $loggerService;

    /**
     * WebServiceLoggerMiddleware constructor.
     *
     * @param LoggerServiceInterface $loggerService
     */
    public function __construct(LoggerServiceInterface $loggerService)
    {
        $this->loggerService = $loggerService;
    }

    /**
     * @inheritDoc
     * @throws Throwable
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $serverParams = $request->getServerParams();
        $clientIp = $serverParams["HTTP_CLIENT_IP"] ?? $serverParams["HTTP_X_FORWARDED_FOR"] ?? null;
        $remoteAddr = $serverParams["REMOTE_ADDR"] ?? null;
        $userAgent = $serverParams["HTTP_USER_AGENT"] ?? null;

        $this->loggerService->createAccessLog(
            new DateTime(),
            $request->getRequestTarget(),
            $request->getMethod(),
            $clientIp,
            $remoteAddr,
            $userAgent
        );

        try {
            $response = $handler->handle($request);
        } catch (Throwable $e) {
            $thrown = $e;
            $this->loggerService->attachErrorLog($thrown);
            $response = new EmptyResponse();
        }

        // Commit access log
        $request->getBody()->rewind();
        $body = $request->getBody()->detach();
        $hasContent = fread($body, 1);
        if ($hasContent) {
            rewind($body);
            $this->loggerService->setAccessLogBody($body);
        }

        $this->loggerService->commit();

        if (isset($thrown)) {
            // Rethrow exception. Let the error handler catch it.
            throw $thrown;
        }

        return $response;
    }
}
