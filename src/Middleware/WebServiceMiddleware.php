<?php


namespace Vallarj\Mezzio\WebService\Middleware;


use Vallarj\Laminas\Rbac\Repository\RbacUserRepositoryInterface;
use Vallarj\Mezzio\OAuth\ResourceServer\Middleware\OAuthResourceServerMiddleware;
use Vallarj\Mezzio\WebService\Entity\AccessLog;
use Vallarj\Mezzio\WebService\Exception\UninitializedAccessLogException;
use Vallarj\Mezzio\WebService\Service\LoggerServiceInterface;
use Mezzio\Router\RouteResult;
use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Vallarj\JsonApi\DecoderInterface;
use Vallarj\JsonApi\EncoderInterface;

class WebServiceMiddleware implements MiddlewareInterface
{
    const ENCODER_ATTRIBUTE = 'web-service-json-api-encoder';
    const DECODER_ATTRIBUTE = 'web-service-json-api-decoder';
    const USER_ATTRIBUTE = 'web-service-user';

    /** @var EncoderInterface */
    private $encoder;

    /** @var DecoderInterface */
    private $decoder;

    /** @var RbacUserRepositoryInterface */
    private $rbacUserRepository;

    /** @var LoggerServiceInterface */
    private $loggerService;

    /** @var bool */
    private $developmentMode;

    /** @var string */
    private $username;

    /** @var string[] */
    private $scopes;

    /**
     * WebServiceMiddleware constructor.
     *
     * @param EncoderInterface $encoder
     * @param DecoderInterface $decoder
     * @param RbacUserRepositoryInterface $rbacUserRepository
     * @param LoggerServiceInterface $loggerService
     * @param bool $developmentMode
     * @param string $username
     * @param array $scopes
     */
    public function __construct(
        EncoderInterface $encoder,
        DecoderInterface $decoder,
        RbacUserRepositoryInterface $rbacUserRepository,
        LoggerServiceInterface $loggerService,
        bool $developmentMode,
        string $username,
        array $scopes
    ) {
        $this->encoder = $encoder;
        $this->decoder = $decoder;
        $this->rbacUserRepository = $rbacUserRepository;
        $this->loggerService = $loggerService;
        $this->developmentMode = $developmentMode;
        $this->username = $username;
        $this->scopes = $scopes;
    }

    /**
     * @inheritDoc
     * @throws UninitializedAccessLogException
     */
    public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface
    {
        $request = $request->withAttribute(self::DECODER_ATTRIBUTE, $this->decoder)
            ->withAttribute(self::ENCODER_ATTRIBUTE, $this->encoder);
        $hasAccessToken = $request->getAttribute(OAuthResourceServerMiddleware::HAS_ACCESS_TOKEN_ATTRIBUTE);
        $hasAccessLog = $this->loggerService->hasAccessLog();
        /** @var RouteResult|null $route */
        $route = $request->getAttribute(RouteResult::class);
        if ($hasAccessLog && $route) {
            $routeName = $route->getMatchedRouteName();
            if ($routeName) {
                $this->loggerService->setAccessLogRouteParameters($routeName, $route->getMatchedParams()['id'] ?? null);
            }
        }

        if ($this->developmentMode && !$hasAccessToken) {
            $user = $this->rbacUserRepository->getBaseUserByUsername($this->username);
            if ($hasAccessLog) {
                $this->loggerService->setAccessLogUser($user);
            }

            return $handler->handle(
                $request->withAttribute(OAuthResourceServerMiddleware::HAS_ACCESS_TOKEN_ATTRIBUTE, true)
                    ->withAttribute(OAuthResourceServerMiddleware::USER_ID_ATTRIBUTE, $user->getId())
                    ->withAttribute(OAuthResourceServerMiddleware::ACCESS_TOKEN_VALID_ATTRIBUTE, true)
                    ->withAttribute(OAuthResourceServerMiddleware::SCOPES_ATTRIBUTE, $this->scopes)
                    ->withAttribute(self::USER_ATTRIBUTE, $user)
            );
        } else {
            $accessTokenValid = $request->getAttribute(OAuthResourceServerMiddleware::ACCESS_TOKEN_VALID_ATTRIBUTE);

            if ($hasAccessToken && $accessTokenValid) {
                $userId = $request->getAttribute(OAuthResourceServerMiddleware::USER_ID_ATTRIBUTE);
                $user = $this->rbacUserRepository->getBaseUser($userId);
            } else {
                $user = null;
            }

            if ($hasAccessLog) {
                $this->loggerService->setAccessLogUser($user);
            }

            return $handler->handle(
                $request->withAttribute(self::USER_ATTRIBUTE, $user)
            );
        }
    }
}
