<?php


namespace Vallarj\Mezzio\WebService;


use Doctrine\ORM\Mapping\Driver\AnnotationDriver;
use Vallarj\Mezzio\WebService\Subscriber\AccessLogUserMapSubscriber;
use Laminas\ServiceManager\Factory\InvokableFactory;

class ConfigProvider
{
    /**
     * Returns the configuration array
     *
     * @return array
     */
    public function __invoke(): array
    {
        return [
            'dependencies' => $this->getDependencies(),
            'php-object-mapper' => $this->getObjectMapperConfiguration(),
            'php-json-api' => $this->getJsonApiConfiguration(),
            'doctrine' => $this->getDoctrineConfiguration()
        ];
    }

    /**
     * Returns the container dependencies
     *
     * @return array
     */
    public function getDependencies(): array
    {
        return [
            'factories' => [
                // Handlers
                Rbac\Handler\PermissionHandler::class => Factory\Rbac\Handler\PermissionHandlerFactory::class,
                Rbac\Handler\RoleHandler::class => Factory\Rbac\Handler\RoleHandlerFactory::class,
                Rbac\Handler\RoleTypeHandler::class => Factory\Rbac\Handler\RoleTypeHandlerFactory::class,
                Rbac\Handler\UserHandler::class => Factory\Rbac\Handler\UserHandlerFactory::class,
                Rbac\Handler\UserHandler\Config::class
                    => Factory\Rbac\Handler\UserHandler\ConfigFactory::class,

                // WebService Middleware
                Middleware\WebServiceMiddleware::class => Factory\Middleware\WebServiceMiddlewareFactory::class,
                Middleware\WebServiceLoggerMiddleware::class
                    => Factory\Middleware\WebServiceLoggerMiddlewareFactory::class,

                // Doctrine subscriber
                Subscriber\AccessLogUserMapSubscriber::class
                    => Factory\Subscriber\AccessLogUserMapSubscriberFactory::class,

                // Mappers
                Rbac\Mapper\PermissionDTOForwardMapper::class => InvokableFactory::class,
                Rbac\Mapper\PermissionDTOReverseMapper::class
                => Factory\Rbac\Mapper\PermissionDTOReverseMapperFactory::class,
                Rbac\Mapper\RoleDTOForwardMapper::class => Factory\Rbac\Mapper\RoleDTOForwardMapperFactory::class,
                Rbac\Mapper\RoleDTOReverseMapper::class => Factory\Rbac\Mapper\RoleDTOReverseMapperFactory::class,
                Rbac\Mapper\RoleTypeDTOForwardMapper::class => InvokableFactory::class,
                Rbac\Mapper\RoleTypeDTOReverseMapper::class
                    => Factory\Rbac\Mapper\RoleTypeDTOReverseMapperFactory::class,

                // Schemas
                Rbac\Schema\PermissionSchema::class => InvokableFactory::class,
                Rbac\Schema\RoleSchema::class => InvokableFactory::class,
                Rbac\Schema\RoleTypeSchema::class => InvokableFactory::class,

                // Services
                Rbac\Service\PermissionService::class => Factory\Rbac\Service\PermissionServiceFactory::class,
                Rbac\Service\RoleService::class => Factory\Rbac\Service\RoleServiceFactory::class,
                Rbac\Service\RoleTypeService::class => Factory\Rbac\Service\RoleTypeServiceFactory::class,
                Rbac\Service\UserService::class => Factory\Rbac\Service\UserServiceFactory::class,
                Service\LoggerService::class => Factory\Service\LoggerServiceFactory::class,
            ],
        ];
    }

    /**
     * Returns php-object-mapper configuration
     *
     * @return array
     */
    public function getObjectMapperConfiguration(): array
    {
        return [
            'mappers' => [
                Rbac\Mapper\PermissionDTOForwardMapper::class,
                Rbac\Mapper\PermissionDTOReverseMapper::class,
                Rbac\Mapper\RoleDTOForwardMapper::class,
                Rbac\Mapper\RoleDTOReverseMapper::class,
                Rbac\Mapper\RoleTypeDTOForwardMapper::class,
                Rbac\Mapper\RoleTypeDTOReverseMapper::class,
            ],
        ];
    }

    /**
     * Returns php-json-api configuration
     *
     * @return array
     */
    public function getJsonApiConfiguration(): array
    {
        return [
            'schemas' => [
                Rbac\Schema\PermissionSchema::class,
                Rbac\Schema\RoleSchema::class,
                Rbac\Schema\RoleTypeSchema::class,
            ],
        ];
    }

    /**
     * Returns Doctrine configuration
     *
     * @return array
     */
    public function getDoctrineConfiguration(): array
    {
        return [
            'driver' => [
                __NAMESPACE__ . '_driver' => [
                    'class' => AnnotationDriver::class,
                    'cache' => 'array',
                    'paths' => [
                        __DIR__ . '/Entity',
                    ],
                ],
                'orm_default' => [
                    'drivers' => [
                        __NAMESPACE__ . '\Entity' => __NAMESPACE__ . '_driver',
                    ],
                ],
            ],
            'eventmanager' => [
                'orm_default' => [
                    'subscribers' => [
                        AccessLogUserMapSubscriber::class
                    ]
                ]
            ]
        ];
    }
}
