<?php


namespace Vallarj\Mezzio\WebService\Subscriber;


use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LoadClassMetadataEventArgs;
use Doctrine\ORM\Events;
use Vallarj\Mezzio\WebService\Entity\AccessLog;

class AccessLogUserMapSubscriber implements EventSubscriber
{
    /** @var string */
    private $userEntity;

    /**
     * AccessLogUserMapSubscriber constructor.
     *
     * @param string $userEntity
     */
    public function __construct(string $userEntity)
    {
        $this->userEntity = $userEntity;
    }

    /**
     * @inheritDoc
     */
    public function getSubscribedEvents(): array
    {
        return [
            Events::loadClassMetadata
        ];
    }

    /**
     * Subscriber on loadClassMetadata
     *
     * @param LoadClassMetadataEventArgs $eventArgs
     * @return void
     */
    public function loadClassMetadata(LoadClassMetadataEventArgs $eventArgs)
    {
        $classMetadata = $eventArgs->getClassMetadata();
        if ($classMetadata->getName() == AccessLog::class) {
            $classMetadata->associationMappings['user']['targetEntity'] = $this->userEntity;
        }
    }
}
