<?php


namespace Vallarj\Mezzio\WebService\Factory\Middleware;


use Vallarj\Mezzio\WebService\Middleware\WebServiceLoggerMiddleware;
use Vallarj\Mezzio\WebService\Service\LoggerService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class WebServiceLoggerMiddlewareFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new WebServiceLoggerMiddleware($container->get(LoggerService::class));
    }
}
