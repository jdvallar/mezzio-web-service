<?php


namespace Vallarj\Mezzio\WebService\Factory\Middleware;


use Vallarj\Mezzio\WebService\Exception\MissingConfigurationException;
use Vallarj\Mezzio\WebService\Middleware\WebServiceMiddleware;
use Vallarj\Mezzio\WebService\Service\LoggerService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Vallarj\JsonApi\Decoder;
use Vallarj\JsonApi\Encoder;

class WebServiceMiddlewareFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        $webServiceConfig = $config['web-service'];
        $rbacUserRepository = $webServiceConfig['rbac']['user-repository'] ?? null;

        if (is_null($rbacUserRepository)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][rbac][user-repository]"
            );
        }

        $devConfig = $webServiceConfig['development'];
        $enabled = (bool)($devConfig['enabled'] ?? false);
        $username = $devConfig['username'] ?? "";
        $scopes = $devConfig['scopes'] ?? [];

        return new WebServiceMiddleware(
            $container->get(Encoder::class),
            $container->get(Decoder::class),
            $container->get($rbacUserRepository),
            $container->get(LoggerService::class),
            $enabled,
            $username,
            $scopes
        );
    }
}
