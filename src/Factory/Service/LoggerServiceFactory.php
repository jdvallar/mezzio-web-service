<?php


namespace Vallarj\Mezzio\WebService\Factory\Service;


use Doctrine\ORM\EntityManager;
use Vallarj\Mezzio\WebService\Service\LoggerService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Ramsey\Uuid\Codec\OrderedTimeCodec;
use Ramsey\Uuid\UuidFactory;

class LoggerServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $uuidFactory = new UuidFactory();
        $codec = new OrderedTimeCodec($uuidFactory->getUuidBuilder());
        $uuidFactory->setCodec($codec);

        return new LoggerService(
            $container->get(EntityManager::class),
            $uuidFactory
        );
    }
}
