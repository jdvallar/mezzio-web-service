<?php


namespace Vallarj\Mezzio\WebService\Factory\Response;


use Vallarj\Mezzio\WebService\Response\InternalServerErrorResponseGenerator;
use Vallarj\Mezzio\WebService\Service\LoggerService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class InternalServerErrorResponseGeneratorFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new InternalServerErrorResponseGenerator($container->get(LoggerService::class));
    }
}
