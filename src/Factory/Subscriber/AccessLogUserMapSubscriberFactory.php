<?php


namespace Vallarj\Mezzio\WebService\Factory\Subscriber;


use Vallarj\Mezzio\WebService\Exception\MissingConfigurationException;
use Vallarj\Mezzio\WebService\Subscriber\AccessLogUserMapSubscriber;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class AccessLogUserMapSubscriberFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        $userEntity = $config['web-service']['entities']['user'] ?? null;

        if (is_null($userEntity)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][entities][user]"
            );
        }

        return new AccessLogUserMapSubscriber($userEntity);
    }
}
