<?php


namespace Vallarj\Mezzio\WebService\Factory\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Repository\PermissionRepository;
use Vallarj\Mezzio\WebService\Rbac\Mapper\PermissionDTOReverseMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PermissionDTOReverseMapperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new PermissionDTOReverseMapper($container->get(PermissionRepository::class));
    }
}
