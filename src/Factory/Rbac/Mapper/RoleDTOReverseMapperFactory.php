<?php


namespace Vallarj\Mezzio\WebService\Factory\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Repository\RoleRepository;
use Vallarj\Mezzio\WebService\Rbac\Mapper\RoleDTOReverseMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RoleDTOReverseMapperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RoleDTOReverseMapper($container->get(RoleRepository::class));
    }
}
