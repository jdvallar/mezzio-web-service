<?php


namespace Vallarj\Mezzio\WebService\Factory\Rbac\Mapper;


use Vallarj\Mezzio\WebService\Exception\MissingConfigurationException;
use Vallarj\Mezzio\WebService\Rbac\Mapper\RoleDTOForwardMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RoleDTOForwardMapperFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        $webServiceConfig = $config['web-service'];
        $rbacUserRepository = $webServiceConfig['rbac']['user-repository'] ?? null;

        if (is_null($rbacUserRepository)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][rbac][user-repository]"
            );
        }

        return new RoleDTOForwardMapper($container->get($rbacUserRepository));
    }
}
