<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Factory\Rbac\Mapper;


use Vallarj\Laminas\Rbac\Repository\RoleTypeRepository;
use Vallarj\Mezzio\WebService\Rbac\Mapper\RoleTypeDTOReverseMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RoleTypeDTOReverseMapperFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RoleTypeDTOReverseMapper($container->get(RoleTypeRepository::class));
    }
}
