<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Factory\Rbac\Handler;


use Vallarj\Mezzio\WebService\Exception\MissingConfigurationException;
use Vallarj\Mezzio\WebService\Rbac\Handler\RoleTypeHandler;
use Vallarj\Mezzio\WebService\Rbac\Service\RoleTypeService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RoleTypeHandlerFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        $webServiceConfig = $config['web-service'];
        $roleReadPermission = $webServiceConfig['rbac']['permissions']['role-read'] ?? null;

        if (is_null($roleReadPermission)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][rbac][permissions][role-read]"
            );
        }

        return new RoleTypeHandler(
            $container->get(RoleTypeService::class),
            $roleReadPermission
        );
    }
}
