<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Factory\Rbac\Handler\UserHandler;


use Vallarj\Mezzio\WebService\Exception\MissingConfigurationException;
use Vallarj\Mezzio\WebService\Rbac\Handler\UserHandler\Config;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class ConfigFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     * @throws MissingConfigurationException
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');
        $webServiceConfig = $config['web-service'];

        $schemas = $config['web-service']['schemas'] ?? [];
        $permissions = $config['web-service']['rbac']['permissions'];
        $userSchema = $schemas['user'] ?? null;
        $adminUserSchema = $schemas['admin-user'] ?? null;

        if (is_null($userSchema)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][schemas][user]"
            );
        }

        if (is_null($adminUserSchema)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][schemas][admin-user]"
            );
        }

        $permissionKeys = [
            'regular-user',
            'user-admin',
            'user-read',
            'user-create',
            'user-update',
            'user-activate',
            'user-deactivate',
            'user-reset-password'
        ];

        foreach ($permissionKeys as $permissionKey) {
            if (!isset($permissions[$permissionKey])) {
                throw new MissingConfigurationException(
                    "Missing WebService config key: [web-service][rbac][permissions][$permissionKey]"
                );
            }
        }

        return new Config(
            $userSchema,
            $adminUserSchema,
            $permissions['regular-user'],
            $permissions['user-admin'],
            $permissions['user-read'],
            $permissions['user-create'],
            $permissions['user-update'],
            $permissions['user-activate'],
            $permissions['user-deactivate'],
            $permissions['user-reset-password']
        );
    }
}
