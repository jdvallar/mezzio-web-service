<?php


namespace Vallarj\Mezzio\WebService\Factory\Rbac\Handler;


use Vallarj\Mezzio\WebService\Exception\MissingConfigurationException;
use Vallarj\Mezzio\WebService\Rbac\Handler\PermissionHandler;
use Vallarj\Mezzio\WebService\Rbac\Service\PermissionService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PermissionHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        $webServiceConfig = $config['web-service'];
        $roleReadPermission = $webServiceConfig['rbac']['permissions']['role-read'] ?? null;

        if (is_null($roleReadPermission)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][rbac][permissions][role-read]"
            );
        }

        return new PermissionHandler(
            $container->get(PermissionService::class),
            $roleReadPermission
        );
    }
}
