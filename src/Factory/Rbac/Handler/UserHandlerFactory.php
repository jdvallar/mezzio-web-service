<?php


namespace Vallarj\Mezzio\WebService\Factory\Rbac\Handler;


use Vallarj\Mezzio\WebService\Rbac\Handler\UserHandler;
use Vallarj\Mezzio\WebService\Rbac\Service\UserService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class UserHandlerFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new UserHandler(
            $container->get(UserService::class),
            $container->get(UserHandler\Config::class)
        );
    }
}
