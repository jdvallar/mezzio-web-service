<?php


namespace Vallarj\Mezzio\WebService\Factory\Rbac\Handler;


use Vallarj\Mezzio\WebService\Exception\MissingConfigurationException;
use Vallarj\Mezzio\WebService\Rbac\Handler\RoleHandler;
use Vallarj\Mezzio\WebService\Rbac\Service\RoleService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RoleHandlerFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        $webServiceConfig = $config['web-service'];
        $roleReadPermission = $webServiceConfig['rbac']['permissions']['role-read'] ?? null;
        $roleManagePermission = $webServiceConfig['rbac']['permissions']['role-manage'] ?? null;

        if (is_null($roleReadPermission)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][rbac][permissions][role-read]"
            );
        }

        if (is_null($roleManagePermission)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][rbac][permissions][role-manage]"
            );
        }

        return new RoleHandler(
            $container->get(RoleService::class),
            $roleReadPermission,
            $roleManagePermission
        );
    }
}
