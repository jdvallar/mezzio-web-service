<?php

declare(strict_types=1);

namespace Vallarj\Mezzio\WebService\Factory\Rbac\Service;


use Vallarj\Laminas\Rbac\Repository\RoleTypeRepository;
use Vallarj\Mezzio\WebService\Rbac\Service\RoleTypeService;
use Vallarj\ObjectMapper\AutoMapper\AutoMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RoleTypeServiceFactory implements FactoryInterface
{
    /**
     * @inheritDoc
     */
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RoleTypeService(
            $container->get(RoleTypeRepository::class),
            $container->get(AutoMapper::class)
        );
    }
}
