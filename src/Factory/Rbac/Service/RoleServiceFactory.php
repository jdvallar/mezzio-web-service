<?php


namespace Vallarj\Mezzio\WebService\Factory\Rbac\Service;


use Doctrine\ORM\EntityManager;
use Vallarj\Laminas\Rbac\Repository\RoleRepository;
use Vallarj\Mezzio\WebService\Rbac\Service\RoleService;
use Vallarj\ObjectMapper\AutoMapper\AutoMapper;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class RoleServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new RoleService(
            $container->get(RoleRepository::class),
            $container->get(AutoMapper::class),
            $container->get(EntityManager::class)
        );
    }
}
