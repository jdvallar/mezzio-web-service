<?php


namespace Vallarj\Mezzio\WebService\Factory\Rbac\Service;


use Vallarj\Laminas\Rbac\Repository\PermissionRepository;
use Vallarj\ObjectMapper\AutoMapper\AutoMapper;
use Vallarj\Mezzio\WebService\Rbac\Service\PermissionService;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;

class PermissionServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        return new PermissionService(
            $container->get(PermissionRepository::class),
            $container->get(AutoMapper::class)
        );
    }
}
