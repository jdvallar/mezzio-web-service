<?php


namespace Vallarj\Mezzio\WebService\Factory\Rbac\Service;


use Doctrine\ORM\EntityManager;
use Vallarj\Laminas\Rbac\Utilities\Password\Bcrypt;
use Vallarj\Mezzio\WebService\Exception\MissingConfigurationException;
use Vallarj\Mezzio\WebService\Rbac\Service\UserService;
use Vallarj\ObjectMapper\AutoMapper\AutoMapper;
use Vallarj\ObjectMapper\Mapper\MapperInterface;
use Interop\Container\ContainerInterface;
use Laminas\ServiceManager\Factory\FactoryInterface;
use Ory\Hydra\Client\Api\AdminApi;

class UserServiceFactory implements FactoryInterface
{
    public function __invoke(ContainerInterface $container, $requestedName, array $options = null)
    {
        $config = $container->get('config');

        $webServiceConfig = $config['web-service'];
        $rbacUserRepository = $webServiceConfig['rbac']['user-repository'] ?? null;
        $usersPageCount = $webServiceConfig['users-page-count'] ?? null;

        $mappers = $config['web-service']['mappers'] ?? [];
        $userDTOForwardMapper = $mappers['user-forward-mapper'] ?? null;
        $userDTOReverseMapper = $mappers['user-reverse-mapper'] ?? null;

        if (is_null($rbacUserRepository)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][rbac][user-repository]"
            );
        }

        if (is_null($userDTOForwardMapper)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][mappers][user-forward-mapper]"
            );
        }

        if (is_null($userDTOReverseMapper)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][mappers][user-reverse-mapper]"
            );
        }

        if (is_null($usersPageCount)) {
            throw new MissingConfigurationException(
                "Missing WebService config key: [web-service][users-page-count]"
            );
        }

        /** @var MapperInterface $userDTOForwardMapper */
        $userDTOForwardMapper = $container->get($userDTOForwardMapper);
        /** @var MapperInterface $userDTOReverseMapper */
        $userDTOReverseMapper = $container->get($userDTOReverseMapper);

        return new UserService(
            $container->get($rbacUserRepository),
            $container->get(AutoMapper::class),
            $container->get(EntityManager::class),
            $container->get(Bcrypt::class),
            $container->get(AdminApi::class),
            $userDTOForwardMapper->getTargetClass(),
            $userDTOReverseMapper->getTargetClass(),
            $usersPageCount
        );
    }
}
